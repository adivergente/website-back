require('dotenv').config()
const jwt = require('jsonwebtoken')
const jwtKey = process.env.JWT_SECRET || 'secret'

const isAuthenticated = function (req, res, next) {
	if (!req.headers.authorization) return res.status(401).send({ success: false, message: 'Unauthenticated', data: { errorLevel: 'token' } })

	const auth = req.headers.authorization.split(' ')

	if (auth[0] === 'Bearer') {
    try {
      const decoded = jwt.verify(auth[1], jwtKey)
      req.userId = decoded.id
      next()
    } catch (err) {
      // return res.status(401).send({ success: false, message: jwtHandleError(err), data: { errorLevel: 'token', errorType: err.name } })
      return res.send({ success: false, message: jwtHandleError(err), data: { errorLevel: 'token', errorType: err.name } })
    }
  } else {
    return res.status(401).send({ success: false, message: 'Autenticación no válida', data: { errorLevel: 'token' } })
  }
}

function jwtHandleError (err) {
  switch(err.name) {
    case 'TokenExpiredError': return 'La sesión ha expirado';
    case 'JsonWebTokenError': return err.message;
    default: return 'Error verificando los datos de sesión';
  }
}

module.exports = isAuthenticated

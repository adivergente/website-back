'use strict'
// Cargamos el módulo de mongoose
var mongoose =  require('mongoose');
// Usaremos los esquemas
var Schema = mongoose.Schema;
// Creamos el objeto del esquema y sus atributos
var OrdenSchema = Schema({
  index: Number,
  folio: String,
  status: String,
  userId: { type: mongoose.Schema.ObjectId, required: true },
  atiende: { type: mongoose.Schema.ObjectId },
  envio: {
  	// direccion: String,
    num: String,
    calle: String,
    colonia: String,
  	codigo_postal: String,
  	estado: String,
    municipio: String,
    localidad: String,
    pais: String,
    referencias: String
  },
  paqueteria: {
  	nombre: { type: String, default: '' },
  	precio: { type: Number, default: 0 },
  	servicio: { type: String, default: '' },
  	tracking_id: { type: String, default: '' }
  },
  forma_pago: String,
  transaction_id: String,
  fecha_pago: {
    type: String,
    default: ''
  },
  comprobante_pago: {
    path: String,
    fecha: String
  },
  productos: [Object],
  subtotal: Number,
  total: Number,
  created_at: String,
  updated_at: String
});
// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('orden', OrdenSchema, 'ordenes');

'use strict'
const { Schema, model } =  require('mongoose')
// const upsertMany = require('@meanie/mongoose-upsert-many')
const aggregatePaginate = require('mongoose-aggregate-paginate-v2')

const opts = {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  // upsertMany: {
  //   matchFields: ['clave_interna'],
  //   type: 'replaceOne',
  //   ensureModel: true
  // },
  collection: 'productos'
}

const productosSchema = new Schema({
  codigo: { type: String, default: '' },
  clave_interna: { type: String, required: true },
  nombre: { type: String, default: '' },
  equivalencias: { type: String, default: '' },
  categoria: { type: String, default: '' },
  subcategoria: { type: String, default: '' },
  proveedor: { type: String, default: '' },
  precio: { type: Number, default: 0 },
  autos: { type: Array, default: [] },
  stock: { type: Number, default: 0 },
  servicio_pesado: { type: Boolean, default: true },
  popoye: { type: Boolean, default: true },
  imagen: { type: Array, default: [] },
  status: { type: Boolean, default: true },
  id_promo: { type: String, default: '' }
  // peso:Number,
  // ancho:Number,
  // alto:Number,
  // largo:Number
}, opts)

// productosSchema.plugin(upsertMany)
productosSchema.plugin(aggregatePaginate)

module.exports = model('Producto', productosSchema)
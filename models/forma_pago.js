'use strict'
// Cargamos el módulo de mongoose
const mongoose =  require('mongoose');
// Usaremos los esquemas
const Schema = mongoose.Schema;
const opts = {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  collection: 'formasPago'
}
// Creamos el objeto del esquema y sus atributos
const FormaPagoSchema = Schema({
  type: { type: String }, // oxxo | deposito | transferencia
  nombre: String,
  banco: { type: String, default: null },
  ref: String
}, opts);
// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('FormaPago', FormaPagoSchema);

  'use strict'
// Cargamos el módulo de mongoose
var mongoose =  require('mongoose');
// Usaremos los esquemas
var Schema = mongoose.Schema;
// Creamos el objeto del esquema y sus atributos
var DevolucionesSchema = Schema({
  id: Number,
  fecha_solicitud: String,
  fecha_atiende: String,
  fecha_cancelacion: String,
  tipo:String,
  id_compra: Number,
  id_prod: String,
  cant:Number,
  precio:Number,
  nombre_cliente: String,
  id_cliente: String,
  atiende:String,
  descripcion: String,
  status:String,
  no_guia:String,
  pago:String 
});
// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('Devoluciones', DevolucionesSchema);

'use strict'
// Cargamos el módulo de mongoose
var mongoose =  require('mongoose')
// Usaremos los esquemas
var Schema = mongoose.Schema

const opts = {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  collection: 'cotizaciones'
}

var CotizacionSchema = Schema({
  pieza: { type: String, required: true },
  marca: { type: String, required: true },
  modelo: String,
  motor: String,
  anio: Number,
  numpart: String,
  status: { type: String, default: 'pendiente' },
  extra: { type: String, default: '' },
  cliente: {
  	nombre: { type: String, required: true },
  	email: { type: String, required: true },
  	telefono: { type: String, default: '' },
  },
  imagen: { type: String, default: '' },
  atiende: { type: mongoose.Schema.ObjectId }
}, opts)

// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('Cotizacion', CotizacionSchema)

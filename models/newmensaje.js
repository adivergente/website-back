  'use strict'
// Cargamos el módulo de mongoose
var mongoose =  require('mongoose');
// Usaremos los esquemas
var Schema = mongoose.Schema;
// Creamos el objeto del esquema y sus atributos
var NewmensajeSchema = Schema({
  id: Number,
  statusChat:String,
  status:String,
  statusVentana:String,
  id_compra:String,
  id_producto:String
});
// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('Newmensaje', NewmensajeSchema);

'use strict'
// Cargamos el módulo de mongoose
var mongoose =  require('mongoose');
// Usaremos los esquemas
var Schema = mongoose.Schema;
// Creamos el objeto del esquema y sus atributos
var CajasSchema = Schema({
  codigo:String,
  alto:Number,
  ancho:Number,
  largo:Number
});
// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('Cajas', CajasSchema);

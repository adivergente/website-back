var nodemailer = require('nodemailer')

var transporter = nodemailer.createTransport({
    protocol: "SMTP",
    host: 'smtp.gmail.com',
    port: 465,
    ssl :  true,
    secure: true, 
    secureOptions: { "rejectUnauthorized": false },
    auth: {
        user: 'reface_web@reface.mx',
        pass: 'Websitereface2021'
    }
})

exports.sendmailPaypal = (to, id) => {
	// console.log('entro a email')
 //  // console.log(cuenta)
  var mailOptions = {
    from: "REFACE <atencion.clientes@reface.com.mx>",
    to: to,
    subject: 'Confirmación de compra',
    html: pagadoHTML(id),
  }

  // regresa promise despues de enviar email
  return transporter.sendMail(mailOptions)
  	.then(info => {
  		return { success: true, info}
  	})
  	.catch(error => {
  		console.log('Error sending mail paypal error: ', error)
  		return { success: false, error }
  	})
 // return pagadoHTML(id)
}

exports.sendmailPagoPendiente = (to, id) => {
  var mailOptions = {
    from: "REFACE <atencion.clientes@reface.com.mx>",
    to: to,
    subject: 'Confirmación de pedido',
    html: pagoPendienteHTML(id),
  }
  return transporter.sendMail(mailOptions)
    .then(info => {
      return { success: true, info}
    })
    .catch(error => {
      console.log('Error sending mail pago pendiente: ', error)
      return { success: false, error }
    })
	// return pagoPendienteHTML(id)
}

exports.sendResetPassword = (to, token) => {
  const mailOptions = {
    from: "REFACE <atencion.clientes@reface.com.mx>",
    to: to,
    subject: 'Recuperación de contraseña',
    html: resetPasswordHTML(token),
  }

  // regresa promise despues de enviar email
  return transporter.sendMail(mailOptions)
    .then(info => {
      console.log('info: ',info)
      return { success: true, info}
    })
    .catch(error => {
      console.log('error: ',error)
      console.log('error: ',error.status)
      return { success: false, error }
    })
}

exports.sendVerificationEmail = (user, token) => {
  var mailOptions = {
    from: "REFACE <atencion.clientes@reface.com.mx>",
    to: user.datos_personales.email,
    subject: 'Confirmación nueva cuenta',
    html: verificationEmailHTML(token),
  }
  return transporter.sendMail(mailOptions)
    .then(info => {
      return { success: true, info}
    })
    .catch(error => {
      console.log('Error sending verificacion de email: ', error)
      return { success: false, error }
    })
  // return verificationEmailHTML(token)
}

function pagadoHTML(id) {
  return "<!DOCTYPE html>\n" +
      "<html>\n" +
      "<head>\n" +
      "\n" +
      "  <meta charset=\"utf-8\">\n" +
      "  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n" +
      "  <title>Password Reset</title>\n" +
      "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
      "  <style type=\"text/css\">\n" +
      "  /**\n" +
      "   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.\n" +
      "   */\n" +
      "  @media screen {\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 400;\n" +
      "      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');\n" +
      "    }\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 700;\n" +
      "      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');\n" +
      "    }\n" +
      "  }\n" +
      "  /**\n" +
      "   * Avoid browser level font resizing.\n" +
      "   * 1. Windows Mobile\n" +
      "   * 2. iOS / OSX\n" +
      "   */\n" +
      "  body,\n" +
      "  table,\n" +
      "  td,\n" +
      "  a {\n" +
      "    -ms-text-size-adjust: 100%; /* 1 */\n" +
      "    -webkit-text-size-adjust: 100%; /* 2 */\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove extra space added to tables and cells in Outlook.\n" +
      "   */\n" +
      "  table,\n" +
      "  td {\n" +
      "    mso-table-rspace: 0pt;\n" +
      "    mso-table-lspace: 0pt;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Better fluid images in Internet Explorer.\n" +
      "   */\n" +
      "  img {\n" +
      "    -ms-interpolation-mode: bicubic;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove blue links for iOS devices.\n" +
      "   */\n" +
      "  a[x-apple-data-detectors] {\n" +
      "    font-family: inherit !important;\n" +
      "    font-size: inherit !important;\n" +
      "    font-weight: inherit !important;\n" +
      "    line-height: inherit !important;\n" +
      "    color: inherit !important;\n" +
      "    text-decoration: none !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Fix centering issues in Android 4.4.\n" +
      "   */\n" +
      "  div[style*=\"margin: 16px 0;\"] {\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  body {\n" +
      "    width: 100% !important;\n" +
      "    height: 100% !important;\n" +
      "    padding: 0 !important;\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Collapse table borders to avoid space between cells.\n" +
      "   */\n" +
      "  table {\n" +
      "    border-collapse: collapse !important;\n" +
      "  }\n" +
      "  a {\n" +
      "    color: #1a82e2;\n" +
      "  }\n" +
      "  p {\n" +
      "    color: #000000;\n" +
      "  }\n" +
      "  img {\n" +
      "    height: auto;\n" +
      "    line-height: 100%;\n" +
      "    text-decoration: none;\n" +
      "    border: 0;\n" +
      "    outline: none;\n" +
      "  }\n" +
      "  </style>\n" +
      "\n" +
      "</head>\n" +
      "<body style=\"background-color: #e9ecef;padding-bottom:30px;\">\n" +
      "\n" +
      "  <!-- start preheader -->\n" +
      "  <div class=\"preheader\" style=\"display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;\">\n" +
      "    \n" +
      "  </div>\n" +
      "  <!-- end preheader -->\n" +
      "\n" +
      "  <!-- start body -->\n" +
      "  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" +
      "\n" +
      "    <!-- start logo -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"center\" valign=\"top\" style=\"padding: 36px 24px;\">\n" +
      "                <img src=\"http://tiendavirtual.dyndns.org:62/assets/images/logo/logo2.png\" alt=\"Reface\" border=\"0\" width=\"120\" style=\"display: block; width: 120px; max-width: 130px; min-width: 120px;\">\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end logo -->\n" +
      "\n" +
      "    <!-- start hero -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;\">\n" +
      "              <h1 style=\"margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;\">Pedido Realizado</h1>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end hero -->\n" +
      "\n" +
      "    <!-- start copy block -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "\n" +
      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">¡Se realizó el pago de tu compra correctamente!</p>\n" +
      "              <p style=\"margin: 0;\">No. de compra: "+id+" </p>\n" +
      "              <p style=\"margin: 0;\">Tu orden estará en camino dentro de las proximas 12 hrs.</p>\n" +
      "              <p style=\"margin: 0;\">Tiempo estimado de entrega: 3 a 5 días.</p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +
      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">Puede hacer el seguimiento de su compra en la siguiente liga</p>\n" +
      "               <a href=\"http://reface.com.mx/mi-cuenta\" target=\"blank\" style=\"display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px;  border-radius: 6px;\">Mi cuenta</a>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +

      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf\">\n" +
      "              <p style=\"margin: 0;\">Atentamente:<br> REFACE</p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end copy block -->\n" +
      "\n" +
      "  </table>\n" +
      "  <!-- end body -->\n" +
      "\n" +
      "</body>\n" +
      "</html>"
}

function pagoPendienteHTML(id) {
  return "<!DOCTYPE html>\n" +
      "<html>\n" +
      "<head>\n" +
      "\n" +
      "  <meta charset=\"utf-8\">\n" +
      "  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n" +
      "  <title>Password Reset</title>\n" +
      "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
      "  <style type=\"text/css\">\n" +
      "  /**\n" +
      "   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.\n" +
      "   */\n" +
      "  @media screen {\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 400;\n" +
      "      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');\n" +
      "    }\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 700;\n" +
      "      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');\n" +
      "    }\n" +
      "  }\n" +
      "  /**\n" +
      "   * Avoid browser level font resizing.\n" +
      "   * 1. Windows Mobile\n" +
      "   * 2. iOS / OSX\n" +
      "   */\n" +
      "  body,\n" +
      "  table,\n" +
      "  td,\n" +
      "  a {\n" +
      "    -ms-text-size-adjust: 100%; /* 1 */\n" +
      "    -webkit-text-size-adjust: 100%; /* 2 */\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove extra space added to tables and cells in Outlook.\n" +
      "   */\n" +
      "  table,\n" +
      "  td {\n" +
      "    mso-table-rspace: 0pt;\n" +
      "    mso-table-lspace: 0pt;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Better fluid images in Internet Explorer.\n" +
      "   */\n" +
      "  img {\n" +
      "    -ms-interpolation-mode: bicubic;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove blue links for iOS devices.\n" +
      "   */\n" +
      "  a[x-apple-data-detectors] {\n" +
      "    font-family: inherit !important;\n" +
      "    font-size: inherit !important;\n" +
      "    font-weight: inherit !important;\n" +
      "    line-height: inherit !important;\n" +
      "    color: inherit !important;\n" +
      "    text-decoration: none !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Fix centering issues in Android 4.4.\n" +
      "   */\n" +
      "  div[style*=\"margin: 16px 0;\"] {\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  body {\n" +
      "    width: 100% !important;\n" +
      "    height: 100% !important;\n" +
      "    padding: 0 !important;\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Collapse table borders to avoid space between cells.\n" +
      "   */\n" +
      "  table {\n" +
      "    border-collapse: collapse !important;\n" +
      "  }\n" +
      "  a {\n" +
      "    color: #1a82e2;\n" +
      "  }\n" +
      "  p {\n" +
      "    color: #000000;\n" +
      "  }\n" +
      "  img {\n" +
      "    height: auto;\n" +
      "    line-height: 100%;\n" +
      "    text-decoration: none;\n" +
      "    border: 0;\n" +
      "    outline: none;\n" +
      "  }\n" +
      "  </style>\n" +
      "\n" +
      "</head>\n" +
      "<body style=\"background-color: #e9ecef;padding-bottom:30px;\">\n" +
      "\n" +
      "  <!-- start preheader -->\n" +
      "  <div class=\"preheader\" style=\"display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;\">\n" +
      "    \n" +
      "  </div>\n" +
      "  <!-- end preheader -->\n" +
      "\n" +
      "  <!-- start body -->\n" +
      "  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" +
      "\n" +
      "    <!-- start logo -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"center\" valign=\"top\" style=\"padding: 36px 24px;\">\n" +
      "                <img src=\"http://tiendavirtual.dyndns.org:62/assets/images/logo/logo2.png\" alt=\"Reface\" border=\"0\" width=\"120\" style=\"display: block; width: 120px; max-width: 130px; min-width: 120px;\">\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end logo -->\n" +
      "\n" +
      "    <!-- start hero -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;\">\n" +
      "              <h1 style=\"margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;\">Solicitud de pedido realizado</h1>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end hero -->\n" +
      "\n" +
      "    <!-- start copy block -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "\n" +
      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">¡Se realizó la orden correctamente!</p>\n" +
      "              <p style=\"margin: 0;\">No. de compra: "+id+" </p>\n" +
      "              <p style=\"margin: 0;\">Favor de mandar su comprobante de pago a la siguiente liga.</p>\n" +
      "               <a href=\"http://reface.com.mx/mi-cuenta?action=adjuntar-comprobante\" target=\"blank\" style=\"display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px;  border-radius: 6px;\">Adjuntar comprobante</a>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +
      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">Si el comprobante de pago ya fue enviado a nosotros, ingresa a la siguiente liga para revisar el estatus de tu compra.</p>\n" +
      "               <a href=\"http://reface.com.mx/mi-cuenta\" target=\"blank\" style=\"display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px;  border-radius: 6px;\">Mi cuenta</a>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +

      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf\">\n" +
      "              <p style=\"margin: 0;\">Atentamente:<br> REFACE</p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end copy block -->\n" +
      "\n" +
      "  </table>\n" +
      "  <!-- end body -->\n" +
      "\n" +
      "</body>\n" +
      "</html>"
}

function resetPasswordHTML(token) {
  return "<!DOCTYPE html>\n" +
      "<html>\n" +
      "<head>\n" +
      "\n" +
      "  <meta charset=\"utf-8\">\n" +
      "  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n" +
      "  <title>Password Reset</title>\n" +
      "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
      "  <style type=\"text/css\">\n" +
      "  /**\n" +
      "   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.\n" +
      "   */\n" +
      "  @media screen {\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 400;\n" +
      "      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');\n" +
      "    }\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 700;\n" +
      "      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');\n" +
      "    }\n" +
      "  }\n" +
      "  /**\n" +
      "   * Avoid browser level font resizing.\n" +
      "   * 1. Windows Mobile\n" +
      "   * 2. iOS / OSX\n" +
      "   */\n" +
      "  body,\n" +
      "  table,\n" +
      "  td,\n" +
      "  a {\n" +
      "    -ms-text-size-adjust: 100%; /* 1 */\n" +
      "    -webkit-text-size-adjust: 100%; /* 2 */\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove extra space added to tables and cells in Outlook.\n" +
      "   */\n" +
      "  table,\n" +
      "  td {\n" +
      "    mso-table-rspace: 0pt;\n" +
      "    mso-table-lspace: 0pt;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Better fluid images in Internet Explorer.\n" +
      "   */\n" +
      "  img {\n" +
      "    -ms-interpolation-mode: bicubic;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove blue links for iOS devices.\n" +
      "   */\n" +
      "  a[x-apple-data-detectors] {\n" +
      "    font-family: inherit !important;\n" +
      "    font-size: inherit !important;\n" +
      "    font-weight: inherit !important;\n" +
      "    line-height: inherit !important;\n" +
      "    color: inherit !important;\n" +
      "    text-decoration: none !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Fix centering issues in Android 4.4.\n" +
      "   */\n" +
      "  div[style*=\"margin: 16px 0;\"] {\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  body {\n" +
      "    width: 100% !important;\n" +
      "    height: 100% !important;\n" +
      "    padding: 0 !important;\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Collapse table borders to avoid space between cells.\n" +
      "   */\n" +
      "  table {\n" +
      "    border-collapse: collapse !important;\n" +
      "  }\n" +
      "  a {\n" +
      "    color: #1a82e2;\n" +
      "  }\n" +
      "  p {\n" +
      "    color: #000000;\n" +
      "  }\n" +
      "  img {\n" +
      "    height: auto;\n" +
      "    line-height: 100%;\n" +
      "    text-decoration: none;\n" +
      "    border: 0;\n" +
      "    outline: none;\n" +
      "  }\n" +
      "  </style>\n" +
      "\n" +
      "</head>\n" +
      "<body style=\"background-color: #e9ecef;padding-bottom:30px;\">\n" +
      "\n" +
      "  <!-- start preheader -->\n" +
      "  <div class=\"preheader\" style=\"display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;\">\n" +
      "    \n" +
      "  </div>\n" +
      "  <!-- end preheader -->\n" +
      "\n" +
      "  <!-- start body -->\n" +
      "  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" +
      "\n" +
      "    <!-- start logo -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"center\" valign=\"top\" style=\"padding: 36px 24px;\">\n" +
      "                <img src=\"http://tiendavirtual.dyndns.org:62/assets/images/logo/logo2.png\" alt=\"Reface\" border=\"0\" width=\"120\" style=\"display: block; width: 120px; max-width: 130px; min-width: 120px;\">\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end logo -->\n" +
      "\n" +
      "    <!-- start hero -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;\">\n" +
      "              <h1 style=\"margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;\">Recuperación de contraseña</h1>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end hero -->\n" +
      "\n" +
      "    <!-- start copy block -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "\n" +
      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">Hemos recibido una solicitud para generar una nueva contraseña, da clic en el boton para que puedas completar el proceso.</p>\n" +
      "              <a href=\"http://reface.com.mx/password-reset/"+token+"\" target=\"blank\" style=\"display: inline-block; padding: 16px 36px; margin-top: 30px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; border-radius: 4px;background: #366cb3; color:#FFFFFF; \">Cambiar contraseña</a>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +

      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf\">\n" +
      "              <p style=\"margin: 0;\">Atentamente:<br> Reface</p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end copy block -->\n" +
      "\n" +
      "  </table>\n" +
      "  <!-- end body -->\n" +
      "\n" +
      "</body>\n" +
      "</html>"
}

function verificationEmailHTML(token) {
  return "<!DOCTYPE html>\n" +
      "<html>\n" +
      "<head>\n" +
      "\n" +
      "  <meta charset=\"utf-8\">\n" +
      "  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n" +
      "  <title>Password Reset</title>\n" +
      "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
      "  <style type=\"text/css\">\n" +
      "  /**\n" +
      "   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.\n" +
      "   */\n" +
      "  @media screen {\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 400;\n" +
      "      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');\n" +
      "    }\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 700;\n" +
      "      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');\n" +
      "    }\n" +
      "  }\n" +
      "  /**\n" +
      "   * Avoid browser level font resizing.\n" +
      "   * 1. Windows Mobile\n" +
      "   * 2. iOS / OSX\n" +
      "   */\n" +
      "  body,\n" +
      "  table,\n" +
      "  td,\n" +
      "  a {\n" +
      "    -ms-text-size-adjust: 100%; /* 1 */\n" +
      "    -webkit-text-size-adjust: 100%; /* 2 */\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove extra space added to tables and cells in Outlook.\n" +
      "   */\n" +
      "  table,\n" +
      "  td {\n" +
      "    mso-table-rspace: 0pt;\n" +
      "    mso-table-lspace: 0pt;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Better fluid images in Internet Explorer.\n" +
      "   */\n" +
      "  img {\n" +
      "    -ms-interpolation-mode: bicubic;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove blue links for iOS devices.\n" +
      "   */\n" +
      "  a[x-apple-data-detectors] {\n" +
      "    font-family: inherit !important;\n" +
      "    font-size: inherit !important;\n" +
      "    font-weight: inherit !important;\n" +
      "    line-height: inherit !important;\n" +
      "    color: inherit !important;\n" +
      "    text-decoration: none !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Fix centering issues in Android 4.4.\n" +
      "   */\n" +
      "  div[style*=\"margin: 16px 0;\"] {\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  body {\n" +
      "    width: 100% !important;\n" +
      "    height: 100% !important;\n" +
      "    padding: 0 !important;\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Collapse table borders to avoid space between cells.\n" +
      "   */\n" +
      "  table {\n" +
      "    border-collapse: collapse !important;\n" +
      "  }\n" +
      "  a {\n" +
      "    color: #1a82e2;\n" +
      "  }\n" +
      "  p {\n" +
      "    color: #000000;\n" +
      "  }\n" +
      "  img {\n" +
      "    height: auto;\n" +
      "    line-height: 100%;\n" +
      "    text-decoration: none;\n" +
      "    border: 0;\n" +
      "    outline: none;\n" +
      "  }\n" +
      "  </style>\n" +
      "\n" +
      "</head>\n" +
      "<body style=\"background-color: #e9ecef;padding-bottom:30px;\">\n" +
      "\n" +
      "  <!-- start preheader -->\n" +
      "  <div class=\"preheader\" style=\"display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;\">\n" +
      "    \n" +
      "  </div>\n" +
      "  <!-- end preheader -->\n" +
      "\n" +
      "  <!-- start body -->\n" +
      "  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" +
      "\n" +
      "    <!-- start logo -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"center\" valign=\"top\" style=\"padding: 36px 24px;\">\n" +
      "                <img src=\"http://tiendavirtual.dyndns.org:62/assets/images/logo/logo2.png\" alt=\"Reface\" border=\"0\" width=\"120\" style=\"display: block; width: 120px; max-width: 130px; min-width: 120px;\">\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end logo -->\n" +
      "\n" +
      "    <!-- start hero -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;\">\n" +
      "              <h1 style=\"margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;\">Confirmación de email</h1>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end hero -->\n" +
      "\n" +
      "    <!-- start copy block -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "\n" +
      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">Bienvenido a REFACE, da clic en el siguiente botón para confirmar tu cuenta de correo.</p>\n" +
      "              <a href=\"http://reface.com.mx/mi-cuenta/verify/"+token+"\" target=\"blank\" style=\"display: inline-block; padding: 16px 36px; margin-top: 30px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; border-radius: 4px;background: #366cb3; color:#FFFFFF; \">Confirmar email</a>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +

      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf\">\n" +
      "              <p style=\"margin: 0;\">Atentamente:<br> Reface</p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +

      // "          <!-- start copy -->\n" +
      // "          <tr>\n" +
      // "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      // "              <p style=\"margin: 0;\"><small><i>Si no puedes dar clic en el botón copia y pega el siguiente enlace en el navegador.</i></small></p>\n" +
      // "              <small>http://reface.com.mx/mi-cuenta/verify/\n"+token+"</small>\n" +
      // "            </td>\n" +
      // "          </tr>\n" +
      // "          <!-- end copy -->\n" +
      "\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end copy block -->\n" +
      "\n" +
      "  </table>\n" +
      "  <!-- end body -->\n" +
      "\n" +
      "</body>\n" +
      "</html>"
}

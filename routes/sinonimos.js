var express = require('express');
var router = express.Router();
var sinonimos = require('../models/sinonimos');

router.get('/list-sinonimos', function(req, res, next) {
    sinonimos.find({ }, function (err, mensaje){
        if(err){
            return res.status(500).send('Error en la peticion');
        }
        if(!sinonimos){
            console.log(sinonimos)
            return res.status(404).send({message: 'Ningun registro identificado'});
        }
        return res.json(sinonimos);
    });
});


module.exports = router;
var express = require('express');
var router = express.Router();
var cajas = require('../models/cajas');

router.get('/list-cajas', function(req, res, next) {
    cajas.find({ }, function (err, cajas){
        if(err){
            console.log('error en caja:',err)
            return res.status(500).send({success:false,message:'Error en la peticion',data:null});
        }
        if(!cajas){
            console.log('cajas',cajas)
            return res.status(404).send({success:false,message: 'Ningun registro identificado',data:null});
        }
        console.log('correcto: ',cajas)
        return res.json({success:true,message: 'Registros encontrados',data:cajas});
    });
});


module.exports = router;
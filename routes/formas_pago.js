const express = require('express')
const router = express.Router()
const FormaPago = require('../models/forma_pago')

router.get('/', (req, res, next) => {
	FormaPago.find({}, (err, fPago) => {
		if (err || !fPago.length) return res.status(404).send({ success: false, message: 'Ningun registro identificado' })
    return res.json({ success: true, message: '', data: multipleFPagoDTO(fPago) })
	})
})

/* Capa DTO */
// Resibe objeto y toma solo esos valores los cuales son los que va a regresar
const singleFPagoDTO = ({ nombre, type, ref, banco }) => ({ nombre, type, ref, banco })

const multipleFPagoDTO = (collection) => collection.map(resource => singleFPagoDTO(resource))

module.exports = router;

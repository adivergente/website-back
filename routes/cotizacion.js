const express = require('express')
const router = express.Router()
const Cotizacion = require('../models/cotizacion')
const path = require('path')
const fs = require('fs')
// const dayjs = require('dayjs')


router.post('/', (req, res, next) => {
  if (!req.body.pieza || !req.body.email) return res.send({ success:false, message: 'Datos incompletos', data: null })

  const validFormats = ['image/png', 'image/jpeg']
  const file = req.files ? req.files.file : null;
  const { nombre, email, telefono } = req.body
  let data = {
    ...req.body,
    cliente: {
      nombre, email, telefono
    }
  }

  if (file) {
    // Validar formato de imagen
    if (!validFormats.includes(file.mimetype)) return res.send({ success: false, message: 'Formato de imagen no válido', data: null })

    const dir = path.normalize(__dirname + '../../../../../replace-sys/imagenes/cotizaciones/')
    fs.mkdir(dir, function(e){
      if(!e || (e && e.code === 'EEXIST')){

        const archivo = fs.writeFileSync(dir + file.name, file.data)
        console.log('archivo: ', archivo)
        data = {
          ...data,
          imagen: file.name
        }
        const cotizacion = new Cotizacion(data)
        cotizacion.save((err) => {
          if (err) return res.send({ success: false, message: err, data: null })
          return res.send({ success:true, message: 'Cotizacion recibida', data: cotizacion }) 
        })            
      } else {
        console.log('no existe: ',e)
        return res.json({ success: false, message: 'Error al guardar imagen', data: [] })
      }
    }) 
  } else {
    const cotizacion = new Cotizacion(data)
    cotizacion.save((err) => {
      if (err) return res.send({ success: false, message: err, data: null })
      return res.send({ success:true, message: 'Cotizacion recibida', data: cotizacion }) 
    })
  }

});

module.exports = router

var express = require('express');
var router = express.Router();
var devoluciones = require('../models/devoluciones');
var newmensaje = require('../models/newmensaje');
var mensaje = require('../models/mensaje');

router.post('/list', function(req, res, next) {
  var cliente = req.body.id
  console.log('cliente: ',cliente)
    newmensaje.find({id_cliente: cliente}, function (err, devoluciones){
        if(err){
            return res.status(500).send({success:false,data:null,message:'Error en la peticion'});
        }
        if(!devoluciones){
            console.log(devoluciones)
            return res.status(404).send({success:false,data:null,message: 'Ningun registro identificado'});
        }
        return res.json({success:true,data:devoluciones,message:"Encontrado"});
    });
});

router.post('/add', function(req, res, next) {
    var data = req.body;
    var f = new Date(); 
    var mes = f.getMonth() + 1
    console.log(String(mes).length)
    if(String(mes).length == 1){
      mes = '0'+mes
    }
    var dia = f.getDate()
    console.log(String(dia).length)
    if(String(dia).length == 1){
      dia = '0'+dia
    }
    var date = f.getFullYear()+'-'+mes+'-'+ dia
    query={
        "id": 1,
        "fecha_solicitud" : date,
        "fecha_atiende" : "",
        "fecha_cancelacion" : "",
        "tipo" : data.tipo,
        "id_compra" :  data.id_compra,
        "id_prod" : data.id_prod,
        "cant" : data.cant,
        "precio" : data.precio,
        "nombre_cliente" : data.nombre_cliente,
        "id_cliente" : data.id_cliente,
        "atiende":"",
        "descripcion":"" ,
        "status":"",
        "no_guia":"",
        "pago":""  
    }
    console.log(data)
    console.log(query)
    
    //metodo para buscar el compras_libre
    devoluciones.create(query)
    .then((rawResponse) =>{
      if(!rawResponse){
        return res.status(404).send({success1: false, success2: false, success3: false, message: 'No se registro', data:null});
      }else{
        var query2 = {    
          "id":1,     
          "statusChat": "Iniciado",
          "status":true,
          "statusVentana":false,
          "id_compra" :  data.id_compra, 
          "id_producto" : data.id_prod
        }
        console.log(query2)
        //return res.status(200).send({success: true, message: 'Registro guardado', data:true});
        newmensaje.create(query2)
        .then((newmensaje) =>{
          console.log("newmensaje: ",newmensaje)
          if(!newmensaje){
            console.log('fallo')
            return res.status(404).send({success1: true, success2: false, success3: false, message: 'No se registro', data:null});
          }else{
            var texto = ""
            if(data.tipo == "D"){
              texto = "Hola que tal, mi nombre es "+data.nombre_cliente+", compre "+data.nombre_producto+", quiero aplicar devolución por: "+data.razon+", buen día"
            }else{
              texto = "Hola que tal, mi nombre es "+data.nombre_cliente+", compre "+data.nombre_producto+", quiero aplicar garantía por: "+data.razon+", buen día"
            }
            var query3 = {
              "id": newmensaje._id,
              "escribe": "C",
              "mensaje": texto,
              "file":""
            }
            mensaje.create(query3)
            .then((mensaje) =>{
              console.log("mensaje: ",mensaje)
              if(!mensaje){
                console.log('fallo')
                return res.status(404).send({success1: true, success2: false, success3: false, message: 'No se registro', data:null});
              }else{
                console.log('Correcto')
                return res.status(200).send({success1: true, success2: true, success3: true,  message: 'Registro guardado', data:newmensaje._id});
              }
            })
          }
        })
        .catch((err) => {
          console.log(err)
          return res.status(500).send({success1: true, success2: false, success3: false, message: 'No se registro', data:null});
        });
      }
    })
    .catch((err) => {
      console.log(err)
      return res.status(500).send({success1: false, success2: false, success3: false, message: 'No se registro', data:null});
    });
  });

module.exports = router;
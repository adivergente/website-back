/*
// Cargamos el módulo de express para poder crear rutas
var express = require('express');
// Cargamos el controlador
var ProductoController = require('../controllers/productos');
// Llamamos al router
var router = express.Router();
// Creamos una ruta para los métodos que tenemos en nuestros controladores
router.get('/productos', ProductoController.getProductByCode);
// Exportamos la configuración
module.exports = router;
*/
var express = require('express');
var router = express.Router();
var producto = require('../models/productos');
var sinonimos = require('../models/sinonimos');
var categoria = require('../models/categorias');
var carrito_compras = require('../models/carrito-compras');
var fs = require('fs');
var path = require('path')

/* GET all products */
router.post('/all', function(req, res, next) {
  producto.aggregate([
    {
      $lookup:{
        from: "promociones",
        localField:"id_promo",
        foreignField:"id_promo", 
        as: "promocion"
      }
    }
    ], function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto){
        //console.log('no hay')
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
        ////console.log('encontro')
        ////console.log(producto.length)
        return res.json({success:true,message:'Encontrados ',data:producto})
      }
  });
});

/* POST all products */
router.post('/all2', function(req, res, next) {
  var specific_data = req.body;
  ////console.log(specific_data)
  var salto = parseInt(specific_data.pagina) - 1
  salto = salto*12
  ////console.log(salto)
  
  producto.find({ }, function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto){
        //console.log('no hay')
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
        //console.log('encontro')
        return res.json({success:true,message:'Encontrados ',data:producto})
      }
  }).limit(12).skip(salto);
});

router.get('/primero', function(req, res, next) {
  //console.log('primero')
  var estructura = [];
  //console.log('segundo')
  var query = {
      "armadora" : {
        "nombre" : "",
        "autos" : [
          {
            "nombre" : "",
            "anios" : [],
            "motor" : []
          }
        ]
      }
  }
  //console.log('tercero')
  estructura.push(query);
  //console.log('cuarto')
  producto.find({ }, function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto){
        //console.log('no hay')
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
        //console.log('encontro')
        ////console.log(producto.length);
        
        for(var a in producto)
        {
          //console.log(a)
          //console.log(producto[a])
          var cadena_temp = producto[a].autos
          //console.log(cadena_temp)
          if(cadena_temp != undefined){
            //console.log('entro')
            var cadena = cadena_temp.toUpperCase();
            ////console.log(cadena)
            var vehiculo_completo = cadena.split('#');
            ////console.log('vehiculo_completo')
            ////console.log(vehiculo_completo)
            var contador_total_vehiculos = vehiculo_completo.length;
            ////console.log('contador_total_vehiculos')
            ////console.log(contador_total_vehiculos)
            if(contador_total_vehiculos > 1){
              for( var contador_while1 in vehiculo_completo)
              {
                var primer_paso_armadora = vehiculo_completo[contador_while1].split('&');
                var armadora = primer_paso_armadora[0];
                ////console.log("armadora: ")
                ////console.log(armadora)

                if( primer_paso_armadora.length > 1 )
                {
                  var primer_paso_carro = primer_paso_armadora[1];
                  ////console.log("primer paso armadora: ")
                  ////console.log(primer_paso_carro)
                  var segundo_paso_carro = primer_paso_carro.split(",");
                  var carro = segundo_paso_carro[0];
                  ////console.log("carro: ")
                  ////console.log(carro)

                  if( segundo_paso_carro.length > 1 )
                  {
                    ////console.log("Consulta con litros y anios")

                    for( var contador_while2 in segundo_paso_carro)
                    {
                      if ( contador_while2 == 0 )
                        continue;

                      var anio = segundo_paso_carro[contador_while2];
                      ////console.log(anio);

                      if( anio.includes("%") == true )
                      {
                        ////console.log("Consulta con todo y todo")
                        var tercer_paso_carro = anio.split("%");
                        var anio_aux = tercer_paso_carro[0];
                        for( var contador_while3 in tercer_paso_carro)
                        {
                          if ( contador_while3 == 0 )
                            continue;

                          var motor = tercer_paso_carro[contador_while3];
                          ////console.log(motor);

                          InsertarArmadoraCocheAnioMotor( armadora, carro, anio_aux, motor, estructura )

                        }

                      }
                      else
                      {
                        InsertarArmadoraCocheAnio( armadora, carro, anio, estructura );
                      }

                    }
                  }
                  else
                  {
                    InsertarArmadoraCoche( armadora, carro, estructura );
                  }
                }
                else
                {
                  InsertarArmadora( armadora, estructura );
                }
              }
            }
          }
        }
        ////console.log("producto armado: ");
        ////console.log(estructura);
        return res.json({success:true,message:'Encontrados ',data:estructura})
      }
  });
});

router.get('/primeros', function(req, res, next) {
  producto.aggregate([
    {
      $lookup:{
        from: "promociones",
        localField:"id_promo",
        foreignField:"id_promo", 
        as: "promocion"
      }
    }
  ], function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto){
        //console.log('no hay')
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
        
        return res.json({success:true,message:'Encontrados ',data:producto})
      }
  }).sort({_id:-1}).limit(8);
});

router.get('/in_stock', function(req, res, next) {
  producto.find({'stock':{$gt: 0}} , function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto){
        //console.log('no hay')
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
        //console.log(producto.length)
        return res.json({success:true,message:'Encontrados ',data:producto})
      }
  });
});

router.get('/nuevos', function(req, res, next) {
  producto.find({ }, function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto){
        //console.log('no hay')
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
       
        return res.json({success:true,message:'Encontrados ',data:producto})
      }
  }).skip(150).limit(8);
});

router.get('/vendidos', function(req, res, next) {

  producto.aggregate([
    {
      $lookup:{
        from: "promociones",
        localField:"id_promo",
        foreignField:"id_promo", 
        as: "promocion"
      }
    }
  ], function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto){
        //console.log('no hay')
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
       
        return res.json({success:true,message:'Encontrados ',data:producto})
      }
  }).skip(200).limit(8);
});

router.get('/general_search/d', function(req, res, next) {
  var code = req.query.search;
  //console.log(code)
  code = code.replace(/á/g, "a")
  code = code.replace(/é/g, "e")
  code = code.replace(/í/g, "i")
  code = code.replace(/ó/g, "o")
  code = code.replace(/ú/g, "u")
  code = code.toUpperCase()
  var arreglo = []
  //console.log('arreglo: ',arreglo)
  /*switch(code.toUpperCase()){
    case 'REPSET':
      arreglo.push('CLUTH')
      arreglo.push('EMBRAGUE')
      break
    case 'CLUTH':
      arreglo.push('EMBRAGUE')
      arreglo.push('REPSET')
      break
    case 'EMBRAGUE':
      arreglo.push('CLUTH')
      arreglo.push('REPSET')
      break
    case 'MAZA':
      arreglo.push('RODAMIENTO')
      arreglo.push('BALERO')
      arreglo.push('COJINETE')
      break
    case 'COJINETE':
      arreglo.push('RODAMIENTO')
      arreglo.push('BALERO')
      arreglo.push('MAZA')
      break
    case 'BALERO':
      arreglo.push('RODAMIENTO')
      arreglo.push('COJINETE')
      arreglo.push('MAZA')
      break
    case 'RODAMIENTO':
      arreglo.push('COJINETE')
      arreglo.push('MAZA')
      arreglo.push('BALERO')
      break
    case 'BATERIA':
      arreglo.push('ACUMULADOR')
      break
    case 'ACUMULADOR':
      arreglo.push('BATERIA')
      break
    case 'BALATA DELANTERA':
      arreglo.push('PASTILLA')
      arreglo.push('BALATA DE DISCO')
      break
    case 'PASTILLA':
      arreglo.push('BALATA DE DISCO')
      arreglo.push('BALATA DELANTERA')
      break
    case 'BALATA DE DISCO':
      arreglo.push('PASTILLA')
      arreglo.push('BALATA DELANTERA')
      break
    case 'BALATA TAMBOR':
      arreglo.push('ZAPATA')
      break
    case 'ZAPATA':
      arreglo.push('BALATA TAMBOR')
      break
    case 'TERMINAL BATERIA':
      arreglo.push('MORDAZA')
      arreglo.push('TERMINAL DE EMERGENCIA')
      break
    case 'MORDAZA':
      arreglo.push('TERMINAL BATERIA')
      arreglo.push('TERMINAL DE EMERGENCIA')
      break
    case 'TERMINAL DE EMERGENCIA':
      arreglo.push('TERMINAL BATERIA')
      arreglo.push('MORDAZA')
      break
    case 'BUJIA':
      arreglo.push('CONECTOR')
      break
    case 'CONECTOR':
      arreglo.push('BUJIA')
      break
    case 'TORNILLO ESTABILIZADOR':
      arreglo.push('ENLASE DE LA BARRA ESTABILIZADORE')
      arreglo.push('CACAHUATE')
      break
    case 'CACAHUATE':
      arreglo.push('ENLASE DE LA BARRA ESTABILIZADORE')
      arreglo.push('TORNILLO ESTABILIZADOR')
      break
    case 'ENLASE DE LA BARRA ESTABILIZADORE':
      arreglo.push('CACAHUATE')
      arreglo.push('TORNILLO ESTABILIZADOR')
      break
    case 'CABEZA':
      arreglo.push('BUJIA')
      break
    case 'BUJIA':
      arreglo.push('CABEZA')
      break
    case 'BIELETA':
      arreglo.push('TERMINAL INTERIOR')
      break
    case 'TERMINAL INTERIOR':
      arreglo.push('BIELETA')
      break
    case 'CUBREPOLVOS':
      arreglo.push('MACHETA')
      arreglo.push('BOTA')
      break
    case 'MACHETA':
      arreglo.push('CUBREPOLVOS')
      arreglo.push('BOTA')
      break
    case 'BOTA':
      arreglo.push('CUBREPOLVOS')
      arreglo.push('MACHETA')
      break
    case 'ESPIGA':
      arreglo.push('JUNTA HOMOSINETICA')
      break
    case 'JUNTA HOMOSINETICA':
      arreglo.push('ESPIGA')
      break
    case 'SOPORTE MOTOR':
      arreglo.push('TACON')
      break
    case 'TACON':
      arreglo.push('SOPORTE MOTOR')
      break
    case 'SOPORTE AMORTIGUADOR':
      arreglo.push('BRIDA')
      arreglo.push('BASE')
      break
    case 'BRIDA':
      arreglo.push('BASE')
      arreglo.push('SOPORTE AMORTIGUADOR')
      break
    case 'BASE':
      arreglo.push('BRIDA')
      arreglo.push('SOPORTE AMORTIGUADOR')
      break
    case 'HORQUILLA DE SUSPENSION':
      arreglo.push('BRAZO')
      break
    case 'BRAZO':
      arreglo.push('HORQUILLA DE SUSPENSION')
      break
    case 'JUNTA':
      arreglo.push('EMPAQUE')
      break
    case 'EMPAQUE':
      arreglo.push('JUNTA')
      break
    case 'SELLO':
      arreglo.push('RETEN')
      break
    case 'RETEN':
      arreglo.push('SELLO')
      break
    default:
      arreglo = replace_search(code)
      break
  }*/
  var consulta1 = {
    $or: [ { 'palabra': { $regex: '.*' + code + '.*' } },
           { 'sinonimos': { $regex: '.*' + code + '.*' } }]
  }
  console.log(code)
  console.log(consulta1)
  sinonimos.find(consulta1, function (err, sinonimos){
    console.log('sinonimos: ',sinonimos)
    if(err){
      console.log('Error en la peticion');
      arreglo = replace_search(code)
      var consulta = []
      for(var i in arreglo){
          consulta.push({ 'descripcion': { $regex: '.*' + arreglo[i] + '.*' } },
        { 'descripcion': { $regex: '.*' + arreglo[i].toUpperCase() + '.*' } },
        { 'descripcion': { $regex: '.*' + arreglo[i].toLowerCase() + '.*' } },
          { 'nombre': { $regex: '.*' + arreglo[i] + '.*' } },
          { 'nombre': { $regex: '.*' + arreglo[i].toUpperCase() + '.*' } },
          { 'nombre': { $regex: '.*' + arreglo[i].toLowerCase() + '.*' } },
          { 'codigo': { $regex: '.*' + arreglo[i].toUpperCase() + '.*' } },
          { 'codigo': { $regex: '.*' + arreglo[i].toLowerCase() + '.*' } })
      }
      query3 = {
        $or:  consulta
      }
      query4 = {
        $or: [ { 'descripcion': { $regex: '.*' + code + '.*' } },
                { 'descripcion': { $regex: '.*' + code.toUpperCase() + '.*' } },
                { 'descripcion': { $regex: '.*' + code.toLowerCase() + '.*' } },
                { 'nombre': { $regex: '.*' + code + '.*' } },
                { 'nombre': { $regex: '.*' + code.toUpperCase() + '.*' } },
                { 'nombre': { $regex: '.*' + code.toLowerCase() + '.*' } }]
      }
      console.log('query')
      console.log(query3)
      console.log(query4)
      //producto.find({ $text: { $search: code }}, function (err, producto){
      producto.aggregate([
      {
        $lookup:{
          from: "promociones",
          localField:"id_promo",
          foreignField:"id_promo", 
          as: "promocion"
        }
      },
      {$match: 
        query3
        
      }])
      .then((rawResponse) =>{
        //console.log(query3)
        //console.log('entro a general')
        if(!rawResponse){

          return res.status(404).send({message: 'Ningun registro identificado'});
        }else{
          //console.log('producto')
          //console.log(rawResponse)
          return res.json({success:true,message:'Encontrados ',data:rawResponse})

        }
      })
      .catch((err) => {
        //console.log(err)
        return res.status(500).send('Error en la peticion');
      });
      //return res.status(500).send('Error en la peticion');
    }
    if(sinonimos.length==0){
      console.log('Ningun registro identificado');
      arreglo = replace_search(code)
      var consulta = []
      for(var i in arreglo){
          consulta.push({ 'descripcion': { $regex: '.*' + arreglo[i] + '.*' } },
        { 'descripcion': { $regex: '.*' + arreglo[i].toUpperCase() + '.*' } },
        { 'descripcion': { $regex: '.*' + arreglo[i].toLowerCase() + '.*' } },
          { 'nombre': { $regex: '.*' + arreglo[i] + '.*' } },
          { 'nombre': { $regex: '.*' + arreglo[i].toUpperCase() + '.*' } },
          { 'nombre': { $regex: '.*' + arreglo[i].toLowerCase() + '.*' } },
          { 'codigo': { $regex: '.*' + arreglo[i].toUpperCase() + '.*' } },
          { 'codigo': { $regex: '.*' + arreglo[i].toLowerCase() + '.*' } })
      }
      query3 = {
        $or:  consulta
      }
      query4 = {
        $or: [ { 'descripcion': { $regex: '.*' + code + '.*' } },
                { 'descripcion': { $regex: '.*' + code.toUpperCase() + '.*' } },
                { 'descripcion': { $regex: '.*' + code.toLowerCase() + '.*' } },
                { 'nombre': { $regex: '.*' + code + '.*' } },
                { 'nombre': { $regex: '.*' + code.toUpperCase() + '.*' } },
                { 'nombre': { $regex: '.*' + code.toLowerCase() + '.*' } }]
      }
      console.log('query')
      console.log(query3)
      console.log(query4)
      //producto.find({ $text: { $search: code }}, function (err, producto){
      producto.aggregate([
      {
        $lookup:{
          from: "promociones",
          localField:"id_promo",
          foreignField:"id_promo", 
          as: "promocion"
        }
      },
      {$match: 
        query3
        
      }])
      .then((rawResponse) =>{
        //console.log(query3)
        //console.log('entro a general')
        if(!rawResponse || !rawResponse.length){

          return res.status(404).send({sucess: false, message: 'Ningun registro identificado', data: null});
        }else{
          console.log('producto')
          console.log(rawResponse)
          return res.json({success:true, message:'Encontrados ', data:rawResponse})

        }
      })
      .catch((err) => {
        //console.log(err)
        return res.status(500).send('Error en la peticion');
      });
      //return res.status(500).send('Error en la peticion');
    }else{  
      //console.log(producto)
      console.log('Encontrados ')
      console.log(sinonimos[0])
      arreglo.push(sinonimos[0].palabra)
      var lista = sinonimos[0].sinonimos.split(",")
      for(var i in lista){
        arreglo.push(lista[i])
      }
      console.log(arreglo)
      var consulta = []
      for(var i in arreglo){
          consulta.push({ 'descripcion': { $regex: '.*' + arreglo[i] + '.*' } },
         { 'descripcion': { $regex: '.*' + arreglo[i].toUpperCase() + '.*' } },
         { 'descripcion': { $regex: '.*' + arreglo[i].toLowerCase() + '.*' } },
          { 'nombre': { $regex: '.*' + arreglo[i] + '.*' } },
          { 'nombre': { $regex: '.*' + arreglo[i].toUpperCase() + '.*' } },
          { 'nombre': { $regex: '.*' + arreglo[i].toLowerCase() + '.*' } },
          { 'codigo': { $regex: '.*' + arreglo[i].toUpperCase() + '.*' } },
          { 'codigo': { $regex: '.*' + arreglo[i].toLowerCase() + '.*' } })
      }
      query3 = {
        $or:  consulta
      }
      query4 = {
        $or: [ { 'descripcion': { $regex: '.*' + code + '.*' } },
                { 'descripcion': { $regex: '.*' + code.toUpperCase() + '.*' } },
                { 'descripcion': { $regex: '.*' + code.toLowerCase() + '.*' } },
                { 'nombre': { $regex: '.*' + code + '.*' } },
                { 'nombre': { $regex: '.*' + code.toUpperCase() + '.*' } },
                { 'nombre': { $regex: '.*' + code.toLowerCase() + '.*' } }]
      }
      console.log('query')
      console.log(query3)
      console.log(query4)
      //producto.find({ $text: { $search: code }}, function (err, producto){
      producto.aggregate([
        {
          $lookup:{
            from: "promociones",
            localField:"id_promo",
            foreignField:"id_promo", 
            as: "promocion"
          }
        },
        {$match: 
          query3
          
      }])
      .then((rawResponse) =>{
        //console.log(query3)
        //console.log('entro a general')
        if(!rawResponse){

          return res.status(404).send({message: 'Ningun registro identificado'});
        }else{
          //console.log('producto')
          //console.log(rawResponse)
          return res.json({success:true,message:'Encontrados ',data:rawResponse})

        }
      })
      .catch((err) => {
        //console.log(err)
        return res.status(500).send('Error en la peticion');
      });
      //return res.status(500).send('Error en la peticion');
    }
        //return res.json(producto);
  });
  /*query = {
    'descripcion': { $regex: '.*' + code.toUpperCase() + '.*' }
    //'nombre': { $regex: '.*' + code.toUpperCase() + '.*' }
  };
  query2 = {
    //'descripcion': { $regex: '.*' + code.toUpperCase() + '.*' }
    'nombre': { $regex: '.*' + code.toLowerCase() + '.*' }
  };*/

});

router.get('/general_search_num/:word', function(req, res, next) {
  var code = req.params.word;
  //console.log(code)
  var palabras = code.split(" ")
  //console.log(palabras.length)
  query = {};
  var array = []
  var array2 = []
  var array3 = []
  var array4 = []
  var cant = 0
    for(var a in palabras){
      var palabra = palabras[a].toLowerCase()
      if((palabra=='de')||(palabra=='el')||(palabra=='los')||(palabra=='la')||(palabra=='las')||(palabra=='tu')||(palabra=='para')||(palabra=='refacciones')||(palabra=='kit')||(palabra=='carro')||(palabra=='repuestos')||(palabra=='y')){
        //console.log('conector')
      }else{
        if(a == 0){
          array.push({'descripcion': { $regex: '.*' + palabra.toUpperCase() + '.*' }})
          array2.push({'descripcion': { $regex: '.*' + palabra.toUpperCase() + '.*' }})          
          array3.push({'autos': { $regex: '.*' + palabra.toUpperCase() + '.*' }})
          array4.push({'autos': { $regex: '.*' + palabra.toUpperCase() + '.*' }})
        }else{
          array.push({'descripcion': { $regex: '.*' + palabra.toUpperCase() + '.*' }})
          array2.push({'autos': { $regex: '.*' + palabra.toUpperCase() + '.*' }})
          array3.push({'descripcion': { $regex: '.*' + palabra.toUpperCase() + '.*' }})
          array4.push({'autos': 'UNIVERSAL' })
        }
      }
      cant++
      //console.log(cant)
    }
    query = {$or : [{$and:array},{$and:array2},{$and:array3}] }

  
  //console.log(query)
    //'nombre': { $regex: '.*' + code.toUpperCase() + '.*' }
  
  //return res.json({success:true,data:'hola'});
  //producto.find({ $text: { $search: code }}, function (err, producto){
    producto.aggregate([
      {
        $lookup:{
          from: "promociones",
          localField:"id_promo",
          foreignField:"id_promo", 
          as: "promocion"
        }
      },
      {$match: 
        query
        
      }])
  .then((rawResponse) =>{
    //console.log(query)
    //console.log('entro a general')
    if(!rawResponse){
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      //console.log('producto')
      
        return res.json({success:true,message:'Encontrados ',data:rawResponse,data2:code})
      
    }
  })
  .catch((err) => {
    //console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

router.get('/promocion', function(req, res, next) {
  query = {
     id_promo: { $ne: "0" }
  };
  console.log('promoción: ', query)
  //producto.find({ $text: { $search: code }}, function (err, producto){
    producto.aggregate([
        {
          $lookup:{
            from: "promociones",
            localField:"id_promo",
            foreignField:"id_promo", 
            as: "promocion"
          }
        },
        {$match: 
          query
          
        }
      ], function (err, producto){
        console.log(err)
        console.log(producto.length)
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto){
        //console.log('no hay')
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
        ////console.log(producto)
        return res.json({success:true,message:'Encontrados ',data:producto})
      }
  }).limit(8);
});

router.get('/vista_promociones', function(req, res, next) {
  query = {
     id_promo: { $ne: "0" }
  };
  console.log('promoción: ', query)
  //producto.find({ $text: { $search: code }}, function (err, producto){
    producto.aggregate([
        {
          $lookup:{
            from: "promociones",
            localField:"id_promo",
            foreignField:"id_promo", 
            as: "promocion"
          }
        },
        {$match: 
          query
          
        }
      ], function (err, producto){
        console.log(err)
        console.log(producto.length)
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto){
        //console.log('no hay')
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
        ////console.log(producto)
        return res.json({success:true,message:'Encontrados ',data:producto})
      }
  })
});

router.get('/general_search2/:word', function(req, res, next) {
  var code = req.params.word;
 
  query = {
    'categoria': code
  };
  //producto.find({ $text: { $search: code }}, function (err, producto){
  producto.find(query, function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto){
        //console.log('no hay')
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
        return res.json({success:true,message:'Encontrados ',data:producto})
      }
  }).limit(4);
});

router.get('/general_search3/:word', function(req, res, next) {
  var code = req.params.word;
  
  query = {
    'categoria': code
  };
  //producto.find({ $text: { $search: code }}, function (err, producto){
  producto.find(query, function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto){
        //console.log('no hay')
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
        return res.json({success:true,message:'Encontrados ',data:producto})
      }
  });
});

/* GET product by code 
  TODO: Cambiar nombre de ruta por show
*/
router.get('/search/p', function(req, res, next) {
  if (!req.query.ref) return res.status(404).send({success: false, data:null, message: 'Ningun registro identificado'})

  const code = req.query.ref
  producto.aggregate([
    {
      $lookup:{
        from: "promociones",
        localField:"id_promo",
        foreignField:"id_promo", 
        as: "promocion"
      }
    },
    {$match: 
      {codigo: code}
      
    }], function (err, producto){
      if(err)
        return res.status(500).send({success: false, data:null, message: 'Error en la peticion'});
      if(!producto)
        return res.status(404).send({success: false, data:null, message: 'Ningun registro identificado'});
    console.log('resultado',producto.length)
    if(producto.length > 0){
      return res.send({success: true, data:producto, message: 'Datos encontrados'});
    }else{
      return res.status(404).send({success: false, data:null, message: 'Ningun registro identificado'});
    }
    
  });
});

/* GET product by code */
router.get('/search2/:code', function(req, res, next) {
  var code = req.params.code;
  producto.aggregate([
    {
      $lookup:{
        from: "promociones",
        localField:"id_promo",
        foreignField:"id_promo", 
        as: "promocion"
      }
    },
    {$match: 
      {clave_interna: code}
      
    }], function (err, producto){
      if(err)
        return res.status(500).send({success: false, data:null, message: 'Error en la peticion'});
      if(!producto)
        return res.status(404).send({success: false, data:null, message: 'Ningun registro identificado'});
    console.log('resultado',producto.length)
    if(producto.length > 0){
      return res.send({success: true, data:producto, message: 'Datos encontrados'});
    }else{
      return res.status(404).send({success: false, data:null, message: 'Ningun registro identificado'});
    }
  });
});

/* GET brands for options */
router.get('/specific/brand', function(req, res, next) {
  producto.find().distinct('marca', function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
    return res.json(producto);
  });
});

/* GET model by brand */
router.get('/specific/categoria', function(req, res, next) {
  producto.find().distinct('categoria', function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
    return res.send(producto);
  });
});

// GET categorias para filtro de busqueda especifica
router.post('/specific-search/categorias', (req, res, next) => {
  const {
    armadora,
    modelo,
    anio,
    motor
  } = req.body;

  // Todos los campos vacios
  if (!armadora && !modelo && !anio && !motor) {
    return res.json({success:false,message:'Ningun registro identificado ',data:[]})
  }

  const query = getQueryForCategoryInSpecificSearch(req.body);

  producto.find(query).distinct('categoria', (err, categorias) => {
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!categorias)
        return res.status(404).send({message: 'Ningun registro identificado'});
    console.log(categorias)
    return res.json({success:true,message:'Encontrados ',data:categorias})
    //return res.json(producto);
  })
})

/* GET model by brand */
router.get('/categoria2', function(req, res, next) {
  categoria.find( function (err, categorias){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!categorias)
        return res.status(404).send({message: 'Ningun registro identificado'});
    return res.send(categorias);
  });
});

/* GET model by brand */
router.get('/categoria_promocion', function(req, res, next) {
 var query = {
    id_promo: { $ne: "0" }
 };
  //producto.find().distinct('categoria', function (err, producto){
    producto.aggregate([
      {
        $lookup:{
          from: "promociones",
          localField:"id_promo",
          foreignField:"id_promo", 
          as: "promocion"
        }
      },
      {$match: 
        query
        
      },
      {
        $group : {
          _id :  "$categoria" 
        }
      }
    ], function (err, producto){
      console.log(err)
      console.log(producto)
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
    return res.send(producto);
  });
});

/* GET model by brand */
router.get('/tipo_promocion/:categoria', function(req, res, next) {
    var categoria = req.params.categoria;
    var query = {
      $and :[{id_promo: { $ne: "0" }},
      {categoria: categoria}]
    };
    console.log(query)
    //producto.find({ 'categoria': categoria }).distinct('tipo', function (err, producto){
    producto.aggregate([
      {
        $lookup:{
          from: "promociones",
          localField:"id_promo",
          foreignField:"id_promo", 
          as: "promocion"
        }
      },
      {$match: 
        query
        
      },
      {
        $group : {
          _id :  "$tipo" 
        }
      }
    ],function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
    return res.send(producto);
  });
});

router.get('/marca_promocion/:tipo', function(req, res, next) {
  var tipo = req.params.tipo;
  //producto.find({ 'tipo': tipo }).distinct('marca', function (err, producto){
    
    var query = {
      $and :[{id_promo: { $ne: "0" }},
      {tipo: tipo}]
    };
    console.log(query)
    producto.aggregate([
      {
        $lookup:{
          from: "promociones",
          localField:"id_promo",
          foreignField:"id_promo", 
          as: "promocion"
        }
      },
      {$match: 
        query
        
      },
      {
        $group : {
          _id :  "$marca" 
        }
      }
    ],function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
    return res.send(producto);
  });
});

/* GET model by brand */
router.get('/specific/pesado', function(req, res, next) {
  producto.aggregate([
    {
      $lookup:{
        from: "promociones",
        localField:"id_promo",
        foreignField:"id_promo", 
        as: "promocion"
      }
    },
    {$match: {
      pesado:'Si'
    }
    }], function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
    console.log(producto)
    return res.json(producto)
    //return res.json(producto);
  });
});

/* GET model by brand */
router.get('/specific/outlet', function(req, res, next) {
  producto.aggregate([
    {
      $lookup:{
        from: "promociones",
        localField:"id_promo",
        foreignField:"id_promo", 
        as: "promocion"
      }
    },
    {$match: {
      outlet:'Si'
    }
    }], function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
    ////console.log(producto)
    return res.json(producto)
    //return res.json(producto);
  });
});

/* GET model by brand */
router.get('/specific/tipo/:categoria', function(req, res, next) {
  var categoria = req.params.categoria;
  producto.find({ 'categoria': categoria }).distinct('tipo', function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
    return res.send(producto);
  });
});

router.post('/specific/categoria2', function(req, res, next) {

  //console.log("entro al metodo ");
  var json_var = req.body;
  //console.log(json_var);
  var query = "";

  var jsonLength = Object.keys(json_var).length;

  //console.log("tamaño");
  //console.log(jsonLength);


  switch (jsonLength) {
    case 1:
      query = {
        $and: [ { autos: { $regex: '.*' + json_var.armadora + '.*' } } ]
      }
      break;
    case 2:
      query = {
        $and: [ { autos: { $regex: '.*' + json_var.armadora + '.*' } },
                { autos: { $regex: '.*' + json_var.auto + '.*' } } ]
      }
      break;
    case 3:
      query = {
        $and: [ { autos: { $regex: '.*' + json_var.armadora + '.*' } },
                { autos: { $regex: '.*' + json_var.auto + '.*' } },
                { autos: { $regex: '.*' + json_var.anio + '.*' } }]
      }
      break;
    case 4:
      query = {
        $and: [ { autos: { $regex: '.*' + json_var.armadora + '.*' } },
                { autos: { $regex: '.*' + json_var.auto + '.*' } },
                { autos: { $regex: '.*' + json_var.anio + '.*' } },
                { autos: { $regex: '.*' + json_var.motor + '.*' } }]
      }
      break;
    default:
  /*
  db.productos.find({
              $and: [ { autos: { $regex: '.*DODGE*' } },
                  { autos: { $regex: '.*D50.*' } },
                  { autos: { $regex: '.*1979.*' } },
                  { autos: { $regex: '.*2.0-2.6L.*' } }]
  })
  */
  }
  //console.log("la query fue: ");
  //console.log(query);

  producto.find( query ).distinct('categoria', function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
      //console.log(producto)
      return res.json({ success:true,message:'Encontrados ',data:producto })
//    return res.send(producto);
  });
});
router.post('/prod_categoria', function(req, res, next) {

  console.log("entro al metodo ");
  var datos = req.body;
  //console.log(json_var);
  var query = {
    'categoria':datos.categoria
  };

  //console.log("la query fue: ");
  console.log(query);

  producto.aggregate([
    {
      $lookup:{
        from: "promociones",
        localField:"id_promo",
        foreignField:"id_promo", 
        as: "promocion"
      }
    },
    {$match: 
      query
      
    }
    ], function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
      //console.log(producto)
      return res.json({ success:true,message:'Encontrados ',data:producto })
//    return res.send(producto);
  });
});
router.get('/specific/marca/:tipo', function(req, res, next) {
  var tipo = req.params.tipo;
  producto.find({ 'tipo': tipo }).distinct('marca', function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
    return res.send(producto);
  });
});

/* GET name by brand */
router.post('/specific/nombre', function(req, res, next) {
  //console.log("entro al metodo de nombre");
 
  var json_var = req.body;
  //console.log(json_var);
  var query = "";
  var jsonLength = Object.keys(json_var).length;

  //console.log("tamaño para nombre");
  //console.log(jsonLength);


  switch (jsonLength) {
    case 2:
      query = {
        $and: [ { autos: { $regex: '.*' + json_var.armadora + '.*' } },
                { 'categoria': json_var.categoria } ]
      }
      break;
    case 3:
      query = {
        $and: [ { autos: { $regex: '.*' + json_var.armadora + '.*' } },
                { autos: { $regex: '.*' + json_var.modelo + '.*' } },
                { 'categoria': json_var.categoria } ]
      }
      break;
    case 4:
      query = {
        $and: [ {autos: { $regex: '.*' + json_var.armadora + '.*' } },
                { autos: { $regex: '.*' + json_var.modelo + '.*' } },
                { autos: { $regex: '.*' + json_var.anio + '.*' } }
                //{ 'categoria': json_var.categoria } ]
              ]
      }
      break;
    case 5:
      query = {
        $and: [ {autos: { $regex: '.*' + json_var.armadora + '.*' } },
                { autos: { $regex: '.*' + json_var.modelo + '.*' } },
                { autos: { $regex: '.*' + json_var.anio + '.*' } },
                { autos: { $regex: '.*' + json_var.motor + '.*' } },
                { 'categoria': json_var.categoria } ]
      }
      break;
  }
  //console.log("la query de nombre fue: ");
  /*query = { $and: [ 
                    { autos: { $regex: '.*' + json_var.armadora + '.*' } }, 
                    { autos: { $regex: '.*' + json_var.modelo + '.*' } }
                  ]}*/
  //console.log(query);
  producto.find(query).distinct('nombre', function (err, producto){
      if(err){
        return res.status(500).send('Error en la peticion');
      }
      if(!producto){
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
        //console.log(producto)
        return res.json({success:true,message:'Encontrados ',data:producto})
      }
  });
});

/* GET full specific query */
router.post('/specific/filtros', function(req, res, next) {
  var specific_data = req.body;

  //console.log("Informacion recibida");
  //console.log(specific_data);

  //var data_size = specific_data.length;
  var cont = 0
  if(specific_data.categoria){
    if(specific_data.tipo){
      if(specific_data.marca){
        cont = 3
        //console.log(cont)
      }else{
        cont = 2
        //console.log(cont)
      }
    }else{
      cont = 1
      //console.log(cont)
    }
  }else{
    if(specific_data.tipo){
        cont = 4
        //console.log(cont)
      
    }
  }
 //console.log(specific_data.categoria)
  var query;

  switch(cont)
  {
    case 1:
      query = {
        'categoria': specific_data.categoria
      };
    break;
    case 2:
      query = {
        'categoria': specific_data.categoria,
        'tipo': specific_data.tipo
      };
    break;
    case 3:
      query = {
        'categoria': specific_data.categoria,
        'tipo': specific_data.tipo,
        'marca': specific_data.marca
      };
    break;
    case 4:
      query = {
        'tipo': specific_data.tipo
      };
    break;
  }


  producto.aggregate([
    {
      $lookup:{
        from: "promociones",
        localField:"id_promo",
        foreignField:"id_promo", 
        as: "promocion"
      }
    },
    {$match: 
      query
      
    }], function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
    ////console.log(producto)
    return res.json({success:true,message:'Encontrados ',data:producto})
    //return res.json(producto);
  });
});

/* GET full specific query */
router.post('/filtros_promocion', function(req, res, next) {
  var specific_data = req.body;
  /*var query = {
    $and :[{id_promo: { $ne: "0" }},
    {tipo: tipo}]
  };*/
  //console.log("Informacion recibida");
  //console.log(specific_data);

  //var data_size = specific_data.length;
  var cont = 0
  if(specific_data.categoria != null ){
    if(specific_data.tipo != null ){
      if(specific_data.marca != null ){
        cont = 3
        //console.log(cont)
      }else{
        cont = 2
        //console.log(cont)
      }
    }else{
      cont = 1
      //console.log(cont)
    }
  }else{
    if(specific_data.tipo != null ){
        cont = 4
        //console.log(cont)
      
    }
  }
 //console.log(specific_data.categoria)
  var query;

  switch(cont)
  {
    case 1:
      query = {
        'categoria': specific_data.categoria,
        id_promo: { $ne: "0" }
      };
    break;
    case 2:
      query = {
        'categoria': specific_data.categoria,
        'tipo': specific_data.tipo,
        id_promo: { $ne: "0" }
      };
    break;
    case 3:
      query = {
        'categoria': specific_data.categoria,
        'tipo': specific_data.tipo,
        'marca': specific_data.marca,
        id_promo: { $ne: "0" }
      };
    break;
    case 4:
      query = {
        'tipo': specific_data.tipo,
        id_promo: { $ne: "0" }
      };
    break;
  }


  producto.aggregate([
    {
      $lookup:{
        from: "promociones",
        localField:"id_promo",
        foreignField:"id_promo", 
        as: "promocion"
      }
    },
    {$match: 
      query 
    }], function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
    ////console.log(producto)
    return res.json({success:true,message:'Encontrados ',data:producto})
    //return res.json(producto);
  });
});

/* GET full specific query */
router.post('/specific2', function(req, res, next) {
  var specific_data = req.body;
  //console.log(specific_data[0].marca)
  query = {
    'marca': specific_data[0].marca
  };
  producto.find(query, function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
    ////console.log(producto)
    return res.json(producto);
  });
});

//metodo para actualizar el stock
router.post('/stockup', function(req, res, next) {
  var data = req.body;
  var new_stock = 0;
  //console.log(data)

  //metodo para buscar el producto
  producto.find({'codigo': data[0].value})
  .then((rawResponse) =>{
    if(!rawResponse){
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      //console.log('correcto')
      var stock = rawResponse[0].stock
      //console.log(stock)
      var quit_stock = data[1].value
      //console.log(quit_stock)
      new_stock = stock - quit_stock
      //console.log(new_stock)

      //metodo que cambia el stock
      producto.updateOne({'codigo': data[0].value}, {'stock': new_stock })
      .then((producto) => {
        //console.log('update')
        ////console.log(producto)

        var result = [{'success':true},{'id':data[0].value},{'stock':data[1].value}]
        ////console.log(result)
        return res.json(result);
      })
      .catch((err) => {
        return res.status(500).send('Error en la peticion');
      });
    }
  })
  .catch((err) => {
    //console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

//metodo para actualizar el stock
router.post('/addstock', function(req, res, next) {
  var data = req.body;
  var new_stock = 0;
  //console.log(data)
  var data = req.body;
  var new_stock = 0;
  //console.log(data[0].clave)
  //console.log(data[1].stock)
  //metodo para buscar el producto
  producto.find({'codigo': data[0].clave})
  .then((rawResponse) =>{
    if(!rawResponse){
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      //console.log('correcto')
      var stock = rawResponse[0].stock
      //console.log(stock)
      var add_stock = data[1].stock
      new_stock = stock + add_stock
      //console.log(new_stock)

      //metodo que cambia el stock
      producto.updateOne({'codigo': data[0].clave}, {'stock': new_stock })
      .then((producto) => {
        //console.log('update')
        //console.log(producto)

        var result = [{'success':true},{'id':data[0].code},{'stock':data[1].stock}]
        ////console.log(result)
        return res.json(result);
      })
      .catch((err) => {
        return res.status(500).send('Error en la peticion');
      });
    }
  })
  .catch((err) => {
    //console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

/*Get categories*/
router.get('/category', function(req, res, next) {
  producto.find().distinct('categoria', function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ninguna categoria identificada'});
    return res.json(producto);
  });
});

/*Get categories*/
router.get('/count', function(req, res, next) {
  producto.count({}, function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto){
        //console.log('no hay')
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
        return res.json({success:true,message:'Encontrados ',data:producto})
      }
  });
});

/* GET full specific query */
router.post('/specific', function(req, res, next) {
  // Separamos en variables para validar
  const {
    armadora,
    modelo,
    anio,
    motor,
    categoria,
    nombre
  } = req.body;

  // Todos los campos vacios
  if (!armadora && !modelo && !anio && !motor && !categoria && !nombre) {
    return res.json({success:false,message:'Ningun registro identificado ',data:[]})
  }

  const query = getQueryForSpecificSearch(req.body);

  producto.aggregate([
    {
      $lookup:{
        from: "promociones",
        localField:"id_promo",
        foreignField:"id_promo", 
        as: "promocion"
      }
    },
    { $match: query }
 ], function (err, producto){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!producto)
        return res.status(404).send({message: 'Ningun registro identificado'});
    console.log(producto)
    return res.json({success:true,message:'Encontrados ',data:producto})
    //return res.json(producto);
  });
});

router.get('/imagenes', function(req, res, next){
  //console.log(req.query.file)
  var url = path.normalize(__dirname + '/../../')+'imagenes_productos/' 
  //console.log(url+req.query.file+'/'+req.query.file)
  //return res.json(url)
  response.senFile(url+req.query.file+'/'+req.query.file+'/'+req.query.file+"-001.jpg")
})

router.get('/recomendados', function(req, res, next) {
  carrito_compras.aggregate([
            {
              $lookup:{
                from: "productos",
                localField:"clave_productos",
                foreignField:"clave_interna", 
                as: "productos"
              }
            },
            {$match: {
              "id_compras": {$ne:""},
              "productos": {$ne:""}
              }
            },
         ], function (err, carrito_compras){
             if(err)
               return res.status(500).send({success:false, data:null,message:'Error en la peticion'});
             if(!carrito_compras)
               return res.status(404).send({success:false, data:null,message:'Error en la peticion'});
           console.log(carrito_compras.length)
           return res.json({success:true, data:carrito_compras,message:'Productos Encontrados'});
         }).sort({cantidades:-1}).limit(10);
         //.limit(15).sort('field -test');
});

/*
* GENERAL FUNCTIONS
*/
function InsertarArmadora( armadora, estructura )
{
  ////console.log("Inserta unicamente armadora");
  var exists = 0;

  for( var a in estructura )
  {
    var armadora_actual = estructura[a].armadora;
    if( armadora_actual.nombre == armadora )
    {
      exists = 1;
      break;
    }
  }

  if( exists == 0 )
  {
    var pushed = {
      "armadora" : {
        "nombre" : armadora,
        "autos" : [
          {
            "nombre" : "",
            "anios" : [],
            "motor" : []
          }
        ]
      }
    }
    estructura.push( pushed );
  }

  ////console.log("Estructura actual: ");
  ////console.log(estructura);
  ////console.log("Hecho");
}

function InsertarArmadoraCoche( armadora, coche, estructura )
{
  var exists_armadora = 0;
  var exists_coche = 0;
  for(var a in estructura)
  {
    var armadora_actual = estructura[a].armadora;
    if( armadora_actual.nombre == armadora )
    {
      exists_armadora = 1;
      for(var b in armadora_actual.autos)
      {
        var auto_actual = armadora_actual.autos[b];
        if( auto_actual.nombre == coche )
        {
          exists_coche = 1;
          break;
        }
      }
      if ( exists_coche == 0 )
      {
        var pushed = {
      //  "autos" : {
            "nombre" : coche,
            "anios" : [],
            "motor" : []
      //  }
        }
        estructura[a].armadora.autos.push( pushed );
      }
      exists_coche = 0;
    }
  }

  if( exists_armadora == 0 )
  {
    var pushed = {
      "armadora" : {
        "nombre" : armadora,
        "autos" : [
          {
            "nombre" : coche,
            "anios" : [],
            "motor" : []
          }
        ]
      }
    }
    estructura.push( pushed );
  }

}

function InsertarArmadoraCocheAnio( armadora, coche, anio, estructura )
{
  var exists_armadora = 0;
  var exists_coche = 0;
  var exists_anio = 0;

  for(var a in estructura)
  {
    var armadora_actual = estructura[a].armadora;
    if( armadora_actual.nombre == armadora )
    {
      exists_armadora = 1;
      for(var b in armadora_actual.autos)
      {
        var auto_actual = armadora_actual.autos[b];
        if( auto_actual.nombre == coche )
        {
          exists_coche = 1;

          for(var c in auto_actual.anios)
          {
            if( auto_actual.anios[c] == anio )
            {
              exists_anio = 1;
              break;
            }
          }

          if( exists_anio == 0 )
          {
            estructura[a].armadora.autos[b].anios.push(anio);
          }
          exists_anio = 0;

        }
      }
      if ( exists_coche == 0 )
      {
        var pushed = {
      //  "autos" : {
            "nombre" : coche,
            "anios" : [anio],
            "motor" : []
      //   }
        }
        estructura[a].armadora.autos.push( pushed );
      }
      exists_coche = 0;
    }
  }

  if( exists_armadora == 0 )
  {
    var pushed = {
      "armadora" : {
        "nombre" : armadora,
        "autos" : [
          {
            "nombre" : coche,
            "anios" : [anio],
            "motor" : []
          }
        ]
      }
    }
    estructura.push( pushed );
  }

}

function InsertarArmadoraCocheAnioMotor( armadora, coche, anio, motor, estructura )
{

  var exists_armadora = 0;
  var exists_coche = 0;
  var exists_anio = 0;
  var exists_motor = 0;

  for(var a in estructura)
  {
    var armadora_actual = estructura[a].armadora;
    if( armadora_actual.nombre == armadora )
    {
      exists_armadora = 1;
      for(var b in armadora_actual.autos)
      {
        var auto_actual = armadora_actual.autos[b];
        if( auto_actual.nombre == coche )
        {

          exists_coche = 1;

          //Verifica si existe el anio del coche
          for(var c in auto_actual.anios)
          {
            if( auto_actual.anios[c] == anio )
            {
              exists_anio = 1;
              break;
            }
          }

          if( exists_anio == 0 )
          {
            estructura[a].armadora.autos[b].anios.push( anio );
          }
          exists_anio = 0;

          //Verifica si existe el motor del coche
          for(var d in auto_actual.motor)
          {
            if( auto_actual.motor[d] == motor )
            {
              exists_motor = 1;
              break;
            }
          }

          if( exists_motor == 0 )
          {
            estructura[a].armadora.autos[b].motor.push( motor );
          }
          exists_motor = 0;

        }
      }
      if ( exists_coche == 0 )
      {
        var pushed = {
      // "autos" : {
            "nombre" : coche,
            "anios" : [anio],
            "motor" : [motor]
      //  }
        }
        estructura[a].armadora.autos.push( pushed );
      }
      exists_coche = 0;
    }
  }

  if( exists_armadora == 0 )
  {
    var pushed = {
      "armadora" : {
        "nombre" : armadora,
        "autos" : [
          {
            "nombre" : coche,
            "anios" : [anio],
            "motor" : [motor]
          }
        ]
      }
    }
    estructura.push( pushed );
  }

}

function replace_search(texto){
  console.log('entro replace')
  var array1 = []
  var array2 = []

  var cont = 0
  var cont2 = 0
  var var1 = ""

  /*texto = texto.replace(/á/g, "a")
  texto = texto.replace(/é/g, "e")
  texto = texto.replace(/í/g, "i")
  texto = texto.replace(/ó/g, "o")
  texto = texto.replace(/ú/g, "u")*/

  

  array1[cont] = texto

  cont++
  console.log('antes de los for: ',texto) 
  for (var i = 0, l = texto.length; i < l; i++) {
    //console.log(texto.charAt(i))
    switch(texto.charAt(i)){
      case "b":
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("b","v")          
          array2[cont2] = array1[a]
          cont2++
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        break
      case "c":
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("c","q")
          array2[cont2] = array1[a]
          cont2++
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("c","s")
          array2[cont2] = array1[a]
          cont2++         
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }          
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("c","k")
          array2[cont2] = array1[a]
          cont2++
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        break
      case "v":
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("v","b")
          array2[cont2] = array1[a]
          cont2++          
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        break
      case "x":
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("x","j")
          array2[cont2] = array1[a]
          cont2++
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        break    
      case "q":
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("q","c")
          array2[cont2] = array1[a]
          cont2++
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("q","k")
          array2[cont2] = array1[a]
          cont2++
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        break
      case "k":
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("k","s")
          array2[cont2] = array1[a]
          cont2++
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("k","q")
          array2[cont2] = array1[a]
          cont2++
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("k","c")
          array2[cont2] = array1[a]
          cont2++
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        break
      case "z":
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("z","s")
          array2[cont2] = array1[a]
          cont2++
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        break
      case "s":
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("s","c")
          array2[cont2] = array1[a]
          cont2++
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        break
      case "y":
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("y","i")
          array2[cont2] = array1[a]
          cont2++
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("y","j")
          array2[cont2] = array1[a]
          cont2++
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("y","ll")
          array2[cont2] = array1[a]
          cont2++
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        break
      case "i":       
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("i","y")
          array2[cont2] = array1[a]
          cont2++          
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        break
      case "l":   
        if(texto.charAt(i+1) == 'l'){
          for(var a = 0; a < array1.length; a++){
            var1 = array1[a].replace("ll","y")
            array2[cont2] = array1[a]
            cont2++          
            if(array2.includes(var1) != true){
              array2[cont2] = var1
              cont2++
            }
          }
          array1 = []
          array1 = array2
          array2 = []
          cont2 = 0
        }
        
        break
      case "g":   
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("g","j")
          array2[cont2] = array1[a]
          cont2++          
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        for(var a = 0; a < array1.length; a++){
          var1 = array1[a].replace("g","y")
          array2[cont2] = array1[a]
          cont2++          
          if(array2.includes(var1) != true){
            array2[cont2] = var1
            cont2++
          }
        }
        array1 = []
        array1 = array2
        array2 = []
        cont2 = 0
        break
      default:
        console.log('entro: default')
        break
    }
  }

  return array1
}

function getQueryForSpecificSearch ({ armadora, modelo, anio, motor, categoria, nombre }) {
  //Caso 1: todos deshabilitados, excepto categoria con nombre
  if(!armadora && !modelo && !anio && !motor && categoria && nombre)
  {
    return { 'categoria': categoria, 'nombre': nombre };
  }
  //Caso 2: solamente la categoria, sin nombre
  if(!armadora && !modelo && !anio && !motor && categoria && !nombre)
  {
    return { 'categoria': categoria };
  }
  //Caso 3: solamente armadora, sin ningun otro filtro de busqueda
  if(armadora && !modelo && !anio && !motor && !categoria && !nombre)
  {
    return { 'autos': { $regex: '.*' + armadora + '.*' } };
  }
  // Mas de un campo no vacio
  else 
  {
    // Array que contendra los filtros a buscar
    let query = [];
    if (armadora) {
      query.push({ 'autos': { $regex: '.*' + armadora + '.*' } })
    }
    if (modelo) {
      query.push({ 'autos': { $regex: '.*' + modelo + '.*' } })
    }
    if (anio) {
      query.push({ 'autos': { $regex: '.*' + anio + '.*' } })
    }
    if (motor) {
      query.push({ 'autos': { $regex: '.*' + motor + '.*' } })
    }
    if (categoria) {
      query.push({ 'categoria': categoria })
    }
    if (nombre) {
      query.push({ 'nombre': nombre })
    }

    return { $and: query }
  }
  // opcion por default
  return {}
}

function getQueryForCategoryInSpecificSearch ({ armadora, modelo, anio, motor }) {
  // solamente armadora, sin ningun otro filtro de busqueda
  if(armadora && !modelo && !anio && !motor)
  {
    return { 'autos': { $regex: '.*' + armadora + '.*' } };
  }
  // Mas de un campo no vacio
  else 
  {
    // Array que contendra los filtros a buscar
    let query = [];
    if (armadora) {
      query.push({ 'autos': { $regex: '.*' + armadora + '.*' } })
    }
    if (modelo) {
      query.push({ 'autos': { $regex: '.*' + modelo + '.*' } })
    }
    if (anio) {
      query.push({ 'autos': { $regex: '.*' + anio + '.*' } })
    }
    if (motor) {
      query.push({ 'autos': { $regex: '.*' + motor + '.*' } })
    }

    return { $and: query }
  }
  // opcion por default
  return {}
}

//this.pdfData = "http://13.52.123.113:84/pdf_venta?file="+response.data.data
module.exports = router;


var express = require('express');
var router = express.Router();
var mensaje = require('../models/mensaje');

router.post('/see', function(req, res, next) {
  var datos = req.body  
  var query = { 'id': datos.id }
  console.log('query: ',query)
  mensaje.find(query, function (err, mensaje){
        console.log('mensaje: ',mensaje)
        if(err){
          console.log('err: ',err)
            return res.status(500).send({success: false, message:'Error en la peticion'});
        }
        if(!mensaje){
            console.log(mensaje)
            return res.status(404).send({success: false,message: 'Ningun registro identificado'});
        }
        return res.status(200).send({success: true, data:mensaje , message: 'Se encontraron registros'});
    });
});

router.post('/add', function(req, res, next) {
  var datos = req.body  
  var query = { 
    "id": datos.id,
    "escribe": datos.escribe,
    "mensaje": datos.mensaje,
    "file":""
  }
  console.log('query: ',query)
  mensaje.create(query)
  .then((mensaje) =>{
    console.log("mensaje: ",mensaje)
    if(!mensaje){
      console.log('fallo')
      return res.status(404).send({success: false,  message: 'No se registro', data:null});
    }else{
      console.log('Correcto')
      return res.status(200).send({success: true,   message: 'Registro guardado', data:null});
    }
  })
});

module.exports = router;
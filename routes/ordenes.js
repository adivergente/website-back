const express = require('express')
const router = express.Router()
const mongoose =  require('mongoose')
const path = require('path')
const fs = require('fs')
const Orden = require('../models/orden')
const Usuario = require('../models/usuarios')
const isAuthenticated = require('../middlewares/authMiddleware')
const dayjs = require('dayjs')
const { sendmailPaypal, sendmailPagoPendiente } = require('../utils/mailing')

/** GET ordenes del usuario */
router.get('/', isAuthenticated, (req, res, next) => {
	console.log(req.userId)
	Orden.aggregate([
			{ $match: { userId: mongoose.Types.ObjectId(req.userId) } },
			{
				$lookup:{
	        from: "usuarios",
	        localField:"userId",
	        foreignField:"_id", 
	        as: "usuario"
	      }
			},
			{ $sort: { created_at: -1 } }
		], (err, ordenes) => {
			if (err) return res.send({ success: false, message: err, data: null })

			console.log('ordenes', ordenes)
			return res.send({ success: true, message: '', data: ordenes })
		})
})

/** POST Nueva orden */
router.post('/', isAuthenticated, (req, res, next) => {
	if (!req.body.productos || !req.body.productos.length) {
		return res.send({ success: false, message: 'No hay productos en tu orden', data: null })
	}
	const fecha = dayjs(new Date()).format('YYYY-MM-DD HH:mm:ss')
	Orden.find({}, (err, response) => {
		if (err) return res.send({ success: false, message: err, data: null })

		const ordenes = response.sort((a, b) => (a.index < b.index) ? 1 : -1)

		// Comparativa para poder inicializar ordenes, despues de segunda orden dara siempre true
		const nextIndex = ordenes.length ? ordenes[0].index + 1 : 1
		const orderData = {
				index: nextIndex,
				folio: `ROL${nextIndex.toString().padStart(6, '0')}`,
				userId: req.userId,
				created_at: fecha,
				updated_at: fecha
			}
		const data = { ...orderData, ...req.body }
		const order = new Orden(data)
		order.save((err) => {
			if (err) return res.send({ success: false, message: err, data: null })
			// enviar email
			Usuario.findById(req.userId, (error, user) => {
				if (error) return res.send({ success:true, message: 'Orden guardada, error al enviar email', data: { error, ...order} })
				if(order.forma_pago === 'paypal') {
					sendmailPaypal(user.datos_personales.email, order.folio)
				} else {
					sendmailPagoPendiente(user.datos_personales.email, order.folio)
				}
			})
			return res.send({ success:true, message: 'Orden guardada', data: order })	
		})
	})
})

router.get('/folio', (req, res, next) => {
	// const timestamp = dayjs(new Date()).valueOf()
	Orden.findOne({}).sort({ folio: -1 })
		.select('folio')
		.then(({ folio }) => {
			return res.send({ data: folio.padStart(6, '0') })
		})
})

/* Post comprobante pago */
router.post('/upload-comprobante', isAuthenticated, (req, res, next) => {
	if (!req.files) return res.send({ success: false, message: 'Error al leer el archivo', data: null })
	const file = req.files.file
	console.log('files', req.files.file)
	console.log('body', req.body)

	// const dir = path.normalize(__dirname+`/../Files/${req.userId}/`)
	const dir = path.normalize(__dirname + `../../../../../replace-sys/comprobantes_pago/${req.userId}/`)
	// console.log(dir)
	fs.mkdir(dir ,function(e){
    if(!e || (e && e.code === 'EEXIST')){

      const archivo = fs.writeFileSync(dir + file.name, file.data)
      console.log('archivo: ', archivo)

      const query = {
      	$set: {
      		'comprobante_pago.path': `${req.userId}/${file.name}`,
      		'comprobante_pago.fecha': dayjs(new Date()).format('YYYY-MM-DD HH:mm:ss')
      	}
      }

      Orden.findOneAndUpdate({ 'folio': req.body.folio }, query, (err, orden) => {
      	if (err || !orden) return res.send({ success: false, message: 'Error al actualizar', data: [] })
      	return res.json({ success:true, message: '¡Gracias hemos recibido tu comprobante de pago!', data: [] })
      })

      // images.forEach(file => {
      //   if (file.image) {
      //     const archivo = fs.writeFileSync(file.path, file.image.data)
      //     console.log('archivo: ',archivo)
      //   }
      // })

      // return res.json({ success:true, message: 'Imagen guardada', data: [] })            
    } else {
      console.log('no existe: ',e);
      return res.json({ success:true, message: 'Error al guardar imagen', data: [] })
    }
  })
	// return res.send({ success: true, message: '', data: dir })
})


// router.get('/test', async (req, res, next) => {
// 	try {
// 		const mailResponse = await sendmailPaypal('martinalanis.dev@gmail.com', 'ROL00015')
// 		console.log('mailresponse:', mailResponse) 
// 		if(mailResponse.success) {
// 			return res.send({ success: true, message: '', data: mailResponse.info })
// 		} else {
// 			return res.send({ success: false, message: '', data: mailResponse.error })
// 		}
// 	} catch (error) {
// 		console.log('erro en get', error)
// 	}
// })

// router.get('/test', isAuthenticated, (req, res, next) => {
// 	Usuario.findById(req.userId, (err, user) => {
// 		if (err) return res.send({ success: false, message: 'Not Found', data: err })
// 		return res.send({ success: true, message: '', data: user.datos_personales.email })
// 	})
// })

module.exports = router

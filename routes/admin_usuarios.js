var express = require('express');
var router = express.Router();
var usuario = require('../models/usuarios');
var usuario2 = require('../models/usuarios');
var shajs = require('sha.js')
var transporter = require('./mailController').transporter

/* GET all users */


router.post('/forget', function(req, res, next) {
  console.log('entro a forget')
  console.log(req.body)
  var id_user = req.body.id_user;
  console.log(id_user)
  //if (request.body.hasOwnProperty("email")){
    let key = randomKey()
    console.log(key)
    var pass = shajs('sha256').update(key).digest('hex')
    usuario.findOne({'id': id_user})
    .then((rawResponse) =>{
      if(!rawResponse){
        console.log(rawResponse)
        return res.status(404).send({message: 'Ningun registro identificado'});
      }else{
        console.log('correcto')
        console.log(rawResponse.datos_personales.nombre_completo)
        //metodo que cambia el stock
        var query = {
          'datos_personales':
          {
            'nombres':          rawResponse.datos_personales.nombres,
            'apellidos':        rawResponse.datos_personales.apellidos,
            'username':         rawResponse.datos_personales.username,
            'email':            rawResponse.datos_personales.email, 
            'password':         pass,
            'telefono':         rawResponse.datos_personales.telefono
          }
        }
        console.log(query)
        //return res.json({success:true,message:'Correo enviado'})
        usuario.updateOne({'id': id_user}, query)
        .then((usuario) => {
          console.log('update')
          console.log(usuario) 
          console.log(rawResponse.datos_personales.nombres)       
          sendEmail(id_user,rawResponse.datos_personales.nombres,key) 
          return res.json({success:true,message:'Correo enviado',data:key})
          
        })
        .catch((err) => {
          return res.status(500).send('Error en la peticion');
        });
      }
    })
    .catch((err) => {
      console.log(err)
      return res.json({success:false,message:'Error en la peticion'})
    });


    /*model.forget(request.body[1].value,request.body[0].value, this.encrypt(key))
        .then(result => {
          if (result.success) {
            console.log('encontro')
            
            callback(result)
          } else {
            console.log('no encontro')
            callback(result)
          }
        })
        .catch(error => {
          callback(error)
        })*/
});
 
function randomKey(){
  let key = ""
  let code = "Aa1Bb2Cc3Dd4Ee5Ff6Gg7Hh8Ii9Jj0Kk!Ll$Mm%Nn&Oo/Pp(Qq)Rr=Ss?Tt¿Uu*Vv+Ww-Xx_Yy<Zz>"

  for (x = 0;x < 12;x++){
    key = key.concat(code.substr(Math.floor(Math.random() * code.length), 1))
  }
  
  return key
}

function sendEmail(to,name,key) {
  var mailOptions = {
    from: "REFACE Password <email@domain.com>",
    to: to,
    subject: "Tu nueva contraseña",
    html: htmlMessageResetPassword(to,key),
  }

  transporter.sendMail(mailOptions, (error, info)=>{
    transporter.close()
    if (error) {
      console.log(error)
      return error
    }else{
      console.log(info)
      return info
    }
  })
}
function htmlMessageResetPassword(email,key) {
  return "<!DOCTYPE html>\n" +
      "<html>\n" +
      "<head>\n" +
      "\n" +
      "  <meta charset=\"utf-8\">\n" +
      "  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n" +
      "  <title>Password Reset</title>\n" +
      "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
      "  <style type=\"text/css\">\n" +
      "  /**\n" +
      "   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.\n" +
      "   */\n" +
      "  @media screen {\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 400;\n" +
      "      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');\n" +
      "    }\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 700;\n" +
      "      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');\n" +
      "    }\n" +
      "  }\n" +
      "  /**\n" +
      "   * Avoid browser level font resizing.\n" +
      "   * 1. Windows Mobile\n" +
      "   * 2. iOS / OSX\n" +
      "   */\n" +
      "  body,\n" +
      "  table,\n" +
      "  td,\n" +
      "  a {\n" +
      "    -ms-text-size-adjust: 100%; /* 1 */\n" +
      "    -webkit-text-size-adjust: 100%; /* 2 */\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove extra space added to tables and cells in Outlook.\n" +
      "   */\n" +
      "  table,\n" +
      "  td {\n" +
      "    mso-table-rspace: 0pt;\n" +
      "    mso-table-lspace: 0pt;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Better fluid images in Internet Explorer.\n" +
      "   */\n" +
      "  img {\n" +
      "    -ms-interpolation-mode: bicubic;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove blue links for iOS devices.\n" +
      "   */\n" +
      "  a[x-apple-data-detectors] {\n" +
      "    font-family: inherit !important;\n" +
      "    font-size: inherit !important;\n" +
      "    font-weight: inherit !important;\n" +
      "    line-height: inherit !important;\n" +
      "    color: inherit !important;\n" +
      "    text-decoration: none !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Fix centering issues in Android 4.4.\n" +
      "   */\n" +
      "  div[style*=\"margin: 16px 0;\"] {\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  body {\n" +
      "    width: 100% !important;\n" +
      "    height: 100% !important;\n" +
      "    padding: 0 !important;\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Collapse table borders to avoid space between cells.\n" +
      "   */\n" +
      "  table {\n" +
      "    border-collapse: collapse !important;\n" +
      "  }\n" +
      "  a {\n" +
      "    color: #1a82e2;\n" +
      "  }\n" +
      "  img {\n" +
      "    height: auto;\n" +
      "    line-height: 100%;\n" +
      "    text-decoration: none;\n" +
      "    border: 0;\n" +
      "    outline: none;\n" +
      "  }\n" +
      "  </style>\n" +
      "\n" +
      "</head>\n" +
      "<body style=\"background-color: #e9ecef;\">\n" +
      "\n" +
      "  <!-- start preheader -->\n" +
      "  <div class=\"preheader\" style=\"display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;\">\n" +
      "    A preheader is the short summary text that follows the subject line when an email is viewed in the inbox.\n" +
      "  </div>\n" +
      "  <!-- end preheader -->\n" +
      "\n" +
      "  <!-- start body -->\n" +
      "  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" +
      "\n" +
      "    <!-- start logo -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"center\" valign=\"top\" style=\"padding: 36px 24px;\">\n" +
      "              <a href=\"https://domain.com/\" target=\"_blank\" style=\"display: inline-block;\">\n" +
      "                <img src=\"http://10.5.0.7:62/assets/images/logo/logo.png\" alt=\"Logo\" border=\"0\" width=\"48\" style=\"display: block; width: 48px; max-width: 48px; min-width: 48px;\">\n" +
      "              </a>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end logo -->\n" +
      "\n" +
      "    <!-- start hero -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;\">\n" +
      "              <h1 style=\"margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;\">Tu contraseña ha cambiado</h1>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end hero -->\n" +
      "\n" +
      "    <!-- start copy block -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "\n" +
      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">Recientemente se solicitó un cambio de contraseña, tus nuevos datos de acceso son los siguientes:</p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +
      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">Usuario : "+email+"</p>\n" +
      "              <p style=\"margin: 0;\">Contraseña : "+key+"</p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +
      "          <!-- start button -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\">\n" +
      "              <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" +
      "                <tr>\n" +
      "                  <td align=\"center\" bgcolor=\"#ffffff\" style=\"padding: 12px;\">\n" +
      "                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" +
      "                      <tr>\n" +
      "                        <td align=\"center\" bgcolor=\"#1a82e2\" style=\"border-radius: 6px;\">\n" +
      "                          <a href=\"http://10.5.0.7:62/login-registro.html\" target=\"_blank\" style=\"display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;\">Iniciar sesión</a>\n" +
      "                        </td>\n" +
      "                      </tr>\n" +
      "                    </table>\n" +
      "                  </td>\n" +
      "                </tr>\n" +
      "              </table>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end button -->\n" +
      "\n" +

      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf\">\n" +
      "              <p style=\"margin: 0;\">Atentamente,<br> Reface</p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end copy block -->\n" +
      "\n" +
      "    <!-- start footer -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\" style=\"padding: 24px;\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "\n" +
      "          <!-- start permission -->\n" +
      "          <tr>\n" +
      "            <td align=\"center\" bgcolor=\"#e9ecef\" style=\"padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;\">\n" +
      "              <p style=\"margin: 0;\"> Recibió este correo electrónico porque solicito la recuperación de contraseña, en caso de dudas o aclaraciones contacte a soporte. No es neceario responder a este correo.</p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end permission -->\n" +
      "\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end footer -->\n" +
      "\n" +
      "  </table>\n" +
      "  <!-- end body -->\n" +
      "\n" +
      "</body>\n" +
      "</html>"
}


router.get('/all-users', function(req, res, next) {
  usuario.find({ }, function (err, usuario){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!usuario)
        return res.status(404).send({message: 'Ningun registro identificado'});
    //console.log(usuario)
    return res.json(usuario);
  });
});


router.get('/all-users/:id', function(req, res, next) {
  var id_user = req.params.id;
  usuario.findOne({ id: id_user }, function (err, usuario){
      if(err)
        return res.status(500).send('Error en la peticion');
      if(!usuario)
        return res.status(404).send({message: 'Ningun registro identificado'});
    console.log(usuario)
    return res.send(usuario);
  });
});


/* GET user by id */
router.post('/datos', function(req, res, next) {
  var datos = req.body;
  console.log(datos)
  usuario.findOne({ id: datos.id }, function (err, usuario){
      if(err){
        return res.send({message:'Error en la peticion'});
      }
      if(!usuario){
        return res.send({message: 'Ningun registro identificado'});
      }else{
        console.log(usuario)
        return res.json({success:true,message:'Encontrado',data:usuario})
      }
  });
});

/* Update user */
router.post('/update-user', function(req, res, next) {

  var specific_data = req.body;
  console.log('Datos de usuario a actualizar')
  console.log(specific_data)
  var id_user = specific_data.id_user
  console.log(id_user)
  //metodo para buscar el usuario
  usuario.findOne({'id': id_user})
  .then((rawResponse) =>{
    if(!rawResponse){
      console.log(rawResponse)
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      console.log('correcto')
      console.log(rawResponse.datos_personales.nombre_completo)
      //metodo que cambia el stock
      var query = {
        'datos_personales':
        {
          'nombres':          specific_data.nombre,
          'apellidos':        specific_data.apellidos,
          'username':         rawResponse.datos_personales.username,
          'email':            specific_data.email, 
          'password':         rawResponse.datos_personales.password,
          'telefono':         specific_data.telefono
        },
        'domicilio':
        {
          'num_interior':     specific_data.num_interior,
          'calle':            specific_data.calle,
          'colonia':          specific_data.colonia,
          'municipio':        specific_data.municipio,
          'estado':           specific_data.estado,
          'pais':             specific_data.pais,
          'codigo_postal':    specific_data.codigo_postal
        }
      }
      console.log(query)
      usuario.updateOne({'id': id_user}, query)
      .then((usuario) => {
        console.log('update')
        console.log(usuario)        
        return res.json({success:true,message:'Datos guardados '})
        
      })
      .catch((err) => {
        return res.status(500).send('Error en la peticion');
      });
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

//delete user
router.post('/delete-user/:id', function(req, res, next) {
  console.log("Entro al metodo")
  var id_user = req.params.id;
  console.log(id_user)
  //metodo para buscar el usuario
  usuario.find({'id': id_user})
  .then((rawResponse) =>{
    if(!rawResponse){
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      console.log('correcto')
      //metodo que cambia el stock
      usuario.updateOne({'id': id_user}, {'status': "Inactivo" })
      .then((usuario) => {
        console.log('update')
        console.log(usuario)
        var result = {'success':true}
        console.log(result)
        return res.json(result);
      })
      .catch((err) => {
        return res.status(500).send('Error en la peticion');
      });
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

/* GET user by id */
router.post('/datos2', function(req, res, next) {
  var datos = req.body;
  console.log(datos)
  usuario.findOne({ id: datos.id }, function (err, usuario){
      if(err){
        return res.send({message:'Error en la peticion'});
      }
      if(!usuario){
        return res.send({message: 'Ningun registro identificado'});
      }else{
        console.log(usuario)
        return res.json({success:true,message:'Encontrado',data:usuario})
      }


  });
});


router.post('/update-user2', function(req, res, next) {

  var specific_data = req.body;

  console.log('Datos de usuario a actualizar')
  console.log(specific_data)
  var id_user = specific_data.id_user
  console.log(id_user)
  //metodo para buscar el usuario
  usuario.findOne({'id': id_user})
  .then((rawResponse) =>{
    if(!rawResponse){
      console.log(rawResponse)
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      console.log('correcto')
      console.log(rawResponse.datos_personales.nombres)
      //metodo que actualiza el usuario
      var query = {
        'datos_personales':
        {
          'nombres':          specific_data.nombre,
          'apellidos':        specific_data.apellidos,
          'password':             specific_data.pass,
          'username':         specific_data.username,
          'fecha_nacimiento': specific_data.fecha_nac,
          'telefono':         specific_data.telefono
        },
        'domicilio':
        {
          'num_interior':     specific_data.num_interior,
          'num_exterior':     specific_data.num_exterior,
          'calle':            specific_data.calle,
          'colonia':          specific_data.colonia,
          'municipio':        specific_data.municipio,
          'estado':           specific_data.estado,
          'pais':             specific_data.pais,
          'codigo_postal':    specific_data.codigo_postal,
          'referencias':      specific_data.referencias
        }
      }
      console.log('query')
      console.log(query)
      
      usuario.updateOne({'id': id_user}, query)
      .then((usuario) => {
        console.log('update')
        console.log(usuario)
        return res.json({success:true,message:'Datos guardados '})

      })
      .catch((err) => {
        return res.status(500).send('Error en la peticion');
      });
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

/* Update user */
router.post('/update-password', function(req, res, next) {

  var specific_data = req.body;

  console.log('Datos de usuario a actualizar');
  console.log(specific_data);

  var id_user = specific_data.id_user;
  var current_password =  shajs('sha256').update( specific_data.password_pre ).digest('hex') ;

  console.log(id_user);
  console.log(current_password);

  var query1 = {
    'id' : id_user,
    /*'datos_personales':
    {
      'password':  current_password
    }*/
  }

  console.log("query para la busqueda");
  console.log(query1);

  //metodo para buscar el usuario
  usuario.findOne( query1 )
  .then((rawResponse) =>{
    if(!rawResponse){
      console.log("no encontro nada");
      console.log(rawResponse);
      return res.status(404).send({message: 'Ningun registro identificado'});
    }else{
      console.log('correcto 1');
      console.log(rawResponse.datos_personales);
      console.log(rawResponse[0]);
      console.log(rawResponse.datos_personales.nombres);
      
      //metodo que actualiza el usuario
      var query = {
        'datos_personales':
        {
          'nombres':          rawResponse.datos_personales.nombres,
          'apellidos':        rawResponse.datos_personales.apellidos,
          'password':         shajs('sha256').update( specific_data.nue_password ).digest('hex'),
          'username':         rawResponse.datos_personales.username,
          'fecha_nacimiento': rawResponse.datos_personales.fecha_nacimiento,
          'telefono':         rawResponse.datos_personales.telefono
        },
        'domicilio':
        {
          'num_interior':     rawResponse.domicilio.num_interior,
          'num_exterior':     rawResponse.domicilio.num_exterior,
          'calle':            rawResponse.domicilio.calle,
          'colonia':          rawResponse.domicilio.colonia,
          'municipio':        rawResponse.domicilio.municipio,
          'estado':           rawResponse.domicilio.estado,
          'pais':             rawResponse.domicilio.pais,
          'codigo_postal':    rawResponse.domicilio.codigo_postal,
          'referencias':      rawResponse.domicilio.referencias
        }
      }
      console.log(query)
      usuario.updateOne({'id': id_user}, query)
      .then((usuario) => {
        console.log('update')
        console.log(usuario)
        return res.json({success:true,message:'Datos guardados '})
      })
      .catch((err) => {
        return res.status(500).send('Error en la peticion');
      });

    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});
module.exports = router;

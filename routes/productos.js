const express = require('express')
const router = express.Router()
const Producto = require('../models/productos')
const Sinonimo = require('../models/sinonimos')

// All
router.get('/', async (req, res) => {
  const sort = parseInt(req.query.sort, 10) || 1
  const { options, aggregate } = createAgreggateForPaginate(req.query)

  /*
    Lmitar resultados donde popoye = true
  */
  if (sort === 5) {
    aggregate.push({
      $match: { popoye: true }
    })
  }
  /*
    Lmitar resultados donde exista promocion
  */
  if (sort === 6) {
    aggregate.push({
      $match: { id_promo: { $ne: '' }}
    })
  }
  
  // console.log(options)
  try {
    /*
      paginate:
      primer parametro es el query para busqueda,
      segundo parametro son para options
     */
    const productAggregate = Producto.aggregate(aggregate)
    const productos = await Producto.aggregatePaginate(productAggregate, options)
    return res.json({ success: true, message: '', data: productos })
  } catch(err){
    return res.status(409).json({ success: false, message: err, data: null })
  }
})

router.get('/show/p', async (req, res) => {
  if (!req.query.ref) return res.status(404).send({success: false, data:null, message: 'Ningun registro identificado'})

  const code = req.query.ref
  
  try {
    const producto = await Producto.aggregate([
      {
        $lookup: {
          from: "promociones",
          localField:"id_promo",
          foreignField:"id_promo", 
          as: "promocion"
        }
      },
      { $match: {codigo: code}}
    ])
    if(!producto) return res.status(404).send({ success: false, data:null, message: 'Ningun registro identificado'})
    if(producto.length > 0){
      return res.send({ success: true, data:producto, message: 'Datos encontrados' })
    }else{
      return res.status(404).send({ success: false, data:null, message: 'Ningun registro identificado' })
    }
  } catch(err) {
    return res.status(409).json({ success: false, message: err, data: null })
  }
})

// Get outlet
router.get('/outlet', async (req, res) => {
  try {
    /*
      paginate:
      primer parametro es el query para busqueda,
      segundo parametro son para options
     */
    const productos = await Producto.aggregate([
      {
        $lookup:{
          from: "promociones",
          localField:"id_promo",
          foreignField:"id_promo", 
          as: "promocion"
        }
      },
      { $match: { popoye: true }}
    ])
    return res.json({ success: true, message: '', data: productos })
  } catch(err){
    return res.status(409).json({ success: false, message: err, data: null })
  }
})

// Get Categorias
router.get('/categorias', async (req, res) => {
  try {
    const categorias = await Producto.find().distinct('categoria')
    // Regresar array sin espacios vacios
    return res.json({ success: true, message: '', data: categorias.filter(i => i.trim()).map(i => i.trim()) })
  } catch(err){
    return res.status(409).json({ success: false, message: err, data: null })
  }
})

// Get primeros 8 productos
router.get('/primeros', async (req, res) => {
  const aggregate = [
    {
      $lookup:{
        from: "promociones",
        localField:"id_promo",
        foreignField:"id_promo", 
        as: "promocion"
      }
    },
    { $sort: { nombre: 1 } },
    { $limit: 8 }
  ]

  try {
    const productos = await Producto.aggregate(aggregate)
    return res.json({ success: true, message: '', data: productos })
  } catch(err){
    return res.status(409).json({ success: false, message: err, data: null })
  }
})

// Get vendidos
router.get('/vendidos', async (req, res) => {
  const aggregate = [
    {
      $lookup:{
        from: "promociones",
        localField:"id_promo",
        foreignField:"id_promo", 
        as: "promocion"
      }
    },
    { $sort: { nombre: 1 } },
    { $skip: 3000 },
    { $limit: 8 }
  ]

  try {
    const productos = await Producto.aggregate(aggregate)
    return res.json({ success: true, message: '', data: productos })
  } catch(err){
    return res.status(409).json({ success: false, message: err, data: null })
  }
})

// Get promocion
router.get('/promocion', async (req, res) => {
  const aggregate = [
    { $match: { id_promo: { $ne: '' }}},
    {
      $lookup:{
        from: "promociones",
        localField:"id_promo",
        foreignField:"id_promo", 
        as: "promocion"
      }
    },
    { $sort: { nombre: 1 } },
    { $limit: 8 }
  ]

  try {
    const productos = await Producto.aggregate(aggregate)
    return res.json({ success: true, message: '', data: productos })
  } catch(err){
    return res.status(409).json({ success: false, message: err, data: null })
  }
})

// Busqueda general
router.get('/search/d', async (req, res) => {
  const originalSearch = req.query.search || null
  if (!originalSearch) return res.status(404).send({success: false, data:null, message: 'No se encontró parametro de busqueda'})

  const search = quitarAcentos(originalSearch).toUpperCase()
  let query = [...createQueryForSearch(originalSearch)]
  // Se encontraron sinonimos
  // query.push(createQueryForSearch(originalSearch))
  
  // const sinonimosRegex = { $regex: `.*${search}.*` }

  try {
    const sinonimosList = await Sinonimo.findOne({ $or: [{ palabra: search }, { sinonimos: { $regex: `.*${search}.*` } }] }).then(res => res && res.sinonimos && res.sinonimos.split(','))
    
    //TODO: corregir otografía en la busqueda, solo si no se encontraron sinonimos ???

    if (sinonimosList && sinonimosList.length) {
      sinonimosList.forEach(synonim => {
        query = [...query, ...createQueryForSearch(synonim, false)]
      })
    }

    const sort = parseInt(req.query.sort, 10) || 1
    const { options, aggregate } = createAgreggateForPaginate(req.query)
    // console.log('aggregate', aggregate)


    //TODO: Agregar match $and para los siguientes casos de sort valide parametro de busqueda y match del sort
    if (sort < 5) {
      aggregate.push({
        $match: { $or:  query }
      })
    } else {
      /*
        Lmitar resultados donde popoye = true
      */
      if (sort === 5) {
        aggregate.push({
          $match: { $and: [
              { $or: query },
              { popoye: true }
            ]}
        })
      }
      /*
        Lmitar resultados donde exista promocion
      */
      if (sort === 6) {
        aggregate.push({
          $match: { $and: [
              { $or: query },
              { id_promo: { $ne: '' }}
            ]}
        })
      }
    }

    
    const productAggregate = Producto.aggregate(aggregate)
    const productos = await Producto.aggregatePaginate(productAggregate, options)
    return res.json({ success: true, message: '', data: productos })
  } catch(err) {
    return res.status(409).json({ success: false, message: 'err try', data: null })
  }
})

router.get('/sp-se/:armadora?/:modelo?/:anios?/:motor?', async (req, res) => {
  const {
    armadora,
    modelo,
    anios,
    motor
  } = req.params

  if(!armadora)
  {
    // Regresar armadoras
    try {
      const list = await Producto.find({}, 'autos')
      const flatten = list.flatMap(item => item.autos)
      const map = [...new Map(flatten.map(item => [
        item.armadora,
        item.armadora
      ])).values()].sort()
      return res.json({ success: false, message: map, data: null })
    } catch (err) {
      return res.status(409).json({ success: false, message: err, data: null })
    }
  }
  else if (armadora && !modelo)
  {
    // Regresar modelos
    try {
      const list = await Producto.find({autos: {$elemMatch: { armadora }}}, 'autos')
      const flatten = list.flatMap(item => item.autos)
      const map = [...new Map(flatten.map(item => {
        if(item.armadora === armadora) {
          return [
            item.modelo,
            item.modelo
          ]
        } else {
          return []
        }
      })).values()].sort().filter(i => i)
      return res.json({ success: false, message: map, data: null })
    } catch (err) {
      return res.status(409).json({ success: false, message: err, data: null })
    }
  }
  else if (armadora && modelo && !anios)
  {
    // regresar años
    const list = await Producto.find({autos: {$elemMatch: { armadora, modelo }}}, 'autos')
    const flatten = list.flatMap(item => item.autos)
    const map = [...new Map(flatten.map(item => {
      if(item.armadora === armadora && item.modelo === modelo) {
        return [
          item.anios,
          item.anios
        ]
      } else {
        return []
      }
    })).values()].sort().filter(i => i)
    const uniques = [...new Set(map.join(',').split(','))].sort()
    return res.json({ success: false, message: uniques, data: null })
  }
  else if (armadora && modelo && anios && !motor)
  {
    // regresar motores
    const list = await Producto.find({
      autos: {
        $elemMatch: {
          armadora,
          modelo,
          anios: { $regex: `.*${anios}.*` }
        }
      }
    }, 'autos')
    const flatten = list.flatMap(item => item.autos)
    const map = [...new Map(flatten.map(item => {
      if(item.armadora === armadora && item.modelo === modelo && item.anios.includes(anios)) {
        return [
          item.motor,
          item.motor
        ]
      } else {
        return []
      }
    })).values()].sort().filter(i => i)
    const uniques = [...new Set(map)].sort()
    return res.json({ success: false, message: map, data: null })
  }
})


/*
  Helpers
*/
function createAgreggateForPaginate ({ page, limit, sort }) {
  const options = {
    page: parseInt(page, 10) || 1,
    limit: parseInt(limit, 10) || 10
  }

  const sortBy = getSortQuery(parseInt(sort, 10) || 1)
  return {
    options,
    aggregate: [
      {
        $lookup:{
          from: "promociones",
          localField:"id_promo",
          foreignField:"id_promo", 
          as: "promocion"
        }
      },
      sortBy
    ]
  }
}

function getSortQuery (type) {
  if(type === 2) return { $sort: { nombre: -1 } }
  if(type === 3) return { $sort: { precio: 1 } }
  if(type === 4) return { $sort: { precio: -1 } }
  /*
    type: undefined || 1 || > 4
  */
  return { $sort: { nombre: 1 } }
}

function quitarAcentos (string) {
  return string.toLowerCase()
    .replace(/á/g, 'a')
    .replace(/é/g, 'e')
    .replace(/í/g, 'i')
    .replace(/ó/g, 'o')
    .replace(/ú/g, 'u')
}

function createQueryForSearch (word, searchInCodigo = true) {
  const arr = [    
    // { 'categoria': { $regex: '.*' + word.toUpperCase() + '.*' }},
    { 'categoria': { $regex: '.*' + word.toLowerCase() + '.*' }},
    // { 'nombre': { $regex: '.*' + word.toUpperCase() + '.*' }},
    { 'nombre': { $regex: '.*' + word.toLowerCase() + '.*' }}
    // { 'codigo': { $regex: '.*' + word.toUpperCase() + '.*' }},
    // { 'codigo': { $regex: '.*' + word.toLowerCase() + '.*' }}
  ]

  // Ingresar la palabra si no se ha ingresado ya
  // if (word !== word.toLowerCase() && word !== word.toUpperCase()) {
  //   arr.push(
  //     { 'categoria': { $regex: '.*' + word + '.*' }},
  //     { 'nombre': { $regex: '.*' + word + '.*' }})
  // }
  // Busca en campo código solo si no se trata de un sinonimo
  if (searchInCodigo) {
    arr.push({ 'codigo': { $regex: '.*' + word.toLowerCase() + '.*' }})
  }
  return arr
}

module.exports = router

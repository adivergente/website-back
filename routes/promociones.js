
var express = require('express')
var router = express.Router()
var Promocion = require('../models/promociones')
const dayjs = require('dayjs')


/* GET all promociones */
router.get('/all', async (req, res, next) => {
  const today = dayjs(new Date()).format('YYYY-MM-DD HH:mm:ss')

  const query = {
    $and: [
      { fecha_in: { $lt: today } },
      { fecha_fin: { $gt: today } },
      { status: "Activo" }
    ]
  }
  console.log(query)
  try {
    return await Promocion.find(query).sort({ fecha_fin: 1 })
      .then(promociones => {
        const message = promociones.length || 'Sin promociones activas'
        const success = promociones.length || false
        return res.send({ success, message, data: promociones })
      })
  } catch (e) {
    console.log(e)
    return res.status(500).send(e)
  }
})

/* GET promocion by code */
router.get('/search/:id_promo', async (req, res, next) => {
  const id_promo = req.params.id_promo
  const today = dayjs(new Date()).format('YYYY-MM-DD HH:mm:ss')

  const query = {
    $and: [
      { id_promo },
      { fecha_in: { $lt: today } },
      { fecha_fin: { $gt: today } },
      { status: "Activo" }
    ]
  }

  try {
    const queryResult = await Promocion.aggregate([
        { $match: query },
        {
          $lookup:{
            from: "productos",
            localField:"id_promo",
            foreignField:"id_promo", 
            as: "productos"
          }
        }
      ]).then(r => r.length ? r[0] : r)

    /**
    * No se encontro promocion
    */
    if (!Object.keys(queryResult).length) return res.send({ success: false, message: 'Sin resultados', data: [] })

    /**
    * Copia de resultado omitiendo productos
    */
    const promocion = [{
      ...queryResult,
      productos: []
    }]

    /**
    * Modificar estructura de los productos agregando data de la promocion a cada registro
    */
    const productsModified = queryResult.productos.map(p => ({ ...p, promocion }))

    return res.send({ success: true, message: '', data: { ...queryResult, productos: productsModified } })
  } catch (e) {
    console.log(e)
    return res.status(500).send(e)
  }
})

module.exports = router;

const express = require('express');
const router = express.Router();
const isAuthenticated = require('../middlewares/authMiddleware')
const fs = require('fs')
const { resolve } = require('path')
const DIR = resolve('estados-municipios')
const { sendmailPaypal, sendmailPagoPendiente } = require('../utils/mailing')

router.get('/estados', isAuthenticated, (req, res, next) => {
  fs.readFile(`${DIR}/Estados.json`, (err, data) => {
    if (err) {
      return res.send({ success: false, message: err, data: [] })
    }
    console.log(JSON.parse(data))
    return res.send({ success: true, message: '', data: JSON.parse(data) })
  })
})

router.get('/municipios/:estado', isAuthenticated, (req, res, next) => {
  const estado = req.params.estado
  if (!estado) {
    return res.send({ success: false, message: 'Nombre de estado no válido', data: [] })
  }
  fs.readFile(`${DIR}/${estado}/Municipios.json`, (err, data) => {
    if (err) {
      return res.send({ success: false, message: 'Error leyendo archivo', data: [] })
    }
    console.log(JSON.parse(data))
    return res.send({ success: true, message: '', data: JSON.parse(data) })
  })
})

/* Localidades o colonias */
router.get('/localidades/:municipio/:estado', isAuthenticated, (req, res, next) => {
  const estado = req.params.estado
  const municipio = req.params.municipio
  console.log(estado, municipio)
  if (!estado || !municipio) {
    return res.send({ success: false, message: 'Estado y municipio requeridos', data: [] })
  }
  fs.readFile(`${DIR}/${estado}/${municipio}.json`, (err, data) => {
    if (err) {
      return res.send({ success: false, message: 'Error leyendo archivo', data: err })
    }
    console.log(JSON.parse(data))
    return res.send({ success: true, message: '', data: JSON.parse(data) })
  })
})

router.get('/server-testing', (req, res) => {
  return res.json({ success: true, message: 'Server online', data: null })
})

// router.get('/mailing', (req, res) => {
//   const html = sendmailPagoPendiente('martinalanis.dev@gmail.com', 'R0015')
//   // res.json({ success: true, message: 'Mailing', data: null })
//   res.send(html)
// })

module.exports = router;

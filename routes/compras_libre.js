/*
// Cargamos el módulo de express para poder crear rutas
var express = require('express');
// Cargamos el controlador
var ProductoController = require('../controllers/productos');
// Llamamos al router
var router = express.Router();
// Creamos una ruta para los métodos que tenemos en nuestros controladores
router.get('/productos', ProductoController.getProductByCode);
// Exportamos la configuración
module.exports = router;
*/
var express = require('express');
var router = express.Router();
var compras_libre = require('../models/compras_libre');
var carrito_compras = require('../models/carrito-compras');
var carrito_compras2 = require('../models/carrito-compras');
var cotizacion = require('../models/cotizacion');
var paypal = require('paypal-rest-sdk')
var fs = require('fs')
var path = require('path')
var transporter = require('./mail-controller').transporter
var builder = require('xmlbuilder');
var request = require('request');
var Base64 = require('js-base64').Base64;
var parser = require('xml2json');
var parseString    =  require ( 'xml2js' ) . parseString ; 
var DomParser = require('dom-parser');
var DOMParser = require('xmldom').DOMParser;
/*Make buy */
router.post('/buy', function(req, res, next) {
  var specific_data = req.body;
  console.log('DATOS DE COMPRA')
  console.log(specific_data)
  var f = new Date();
  var query = {
    'id':specific_data.id,
    'estado':specific_data.estado,
    'total_productos':specific_data.total_productos,
    'total_venta':specific_data.total_venta,
    'fecha':new Date(f.getFullYear(),f.getMonth() +1,f.getDate(),f.getHours(),f.getMinutes(),f.getSeconds()).toISOString(),
    'datos_personales':
    {
      'nombre_completo': specific_data.nombre,
      'email': specific_data.email,
      'telefono': specific_data.telefono
    },
    'tipo': specific_data.tipo,
    'envio': specific_data.envio,
    'numero_guia':specific_data.guia,
    'datos_entrega':
    {
      'codigo_postal': specific_data.codigo_postal,
      'estado': specific_data.estado,
      'municipio': specific_data.municipio,
      'calle': specific_data.calle,
      'num_interior': specific_data.num_interior,
      'num_exterior': '',
      'colonia': specific_data.colonia,
      'referencias': specific_data.referencias
    },
    'imagen':'',
    'atiende':''
  }
  console.log(query)
  //return res.json({success:true,message:'Datos',data:query});
  compras_libre.create( query, function (err, compras_libre){
    //console.log('update')
    //console.log(query)
    console.log(compras_libre)
    if(!compras_libre){
      return res.json({success:false,message:'No se guardaron los datos'});
    }
    else{
      return res.json({success:true,message:'Se guardaron los datos',data:compras_libre});

    }
  });
});

router.post('/apartar', function(req, res, next) {
  var specific_data = req.body;
  console.log('DATOS DE CARRITO')
  console.log(specific_data)
  var query = {
    "id_usuario": specific_data.id_usuario,
    'clave_productos': specific_data.clave_producto
  }
  var query2 = {
    "id_compras": specific_data.id,    
  }
  console.log('DATOS')
  console.log(query)
  console.log(query2)
  //return res.json({success:true,message:'Encontro '})
  carrito_compras.updateOne(query, query2,function (err, carrito_compras){
    if(err)
      return res.status(500).send('Error en la peticion');
    if(!carrito_compras){
      return res.json({success:false,message:'No encontro '})
    }else{
      console.log(carrito_compras)
      return res.json({success:true,message:'Producto apartado'})
    }
  });
});

router.post('/oxxo', function(req, res, next) {
  var specific_data = req.body;
  console.log('DATOS DE OXXO')
  console.log(specific_data)
  var user = specific_data.id_user
  var id_compra = specific_data.id
  var query = {
    "id_usuario": user,
    'id_compras': id_compra,
    'cuenta':  specific_data.cuenta,
    'banco':  specific_data.banco
  }
  console.log('DATOS')
  console.log(query)
  //return res.json({success:true,message:'Encontro '})
  carrito_compras.find(query,function (err, carrito_compras){
    if(err)
      return res.status(500).send('Error en la peticion');
    if(!carrito_compras){
      return res.json({success:false,message:'No encontro '})
    }else{
      console.log(carrito_compras)
      console.log(id_compra.toString().length)
      var folio = 'ROL'+id_compra;      
      sendEmail3(user,carrito_compras,query,folio) 
      return res.json({success:true,message:'Correo enviado'})
      //return res.json({success:true,message:'Encontrado'})
    }
  });
});

router.post('/paypal', function(req, res, next) {
  var specific_data = req.body;
  console.log('DATOS DE Paypal')
  console.log(specific_data)
  var user = specific_data.id_user
  var id_compra = specific_data.id
  var query = {
    "id_usuario": user,
    'id_compras': id_compra,
    'cuenta':  specific_data.cuenta,
    'banco':  specific_data.banco
  }
  console.log('DATOS')
  console.log(query)
  //return res.json({success:true,message:'Encontro '})
  carrito_compras.find(query,function (err, carrito_compras){
    if(err)
      return res.status(500).send('Error en la peticion');
    if(!carrito_compras){
      return res.json({success:false,message:'No encontro '})
    }else{
      console.log(carrito_compras)
      console.log(id_compra.toString().length)
      var folio = ''
      switch(id_compra.toString().length){
        case 1:
          folio = 'Reface0000'+id_compra;
          break;
        case 2:
          folio = 'Reface000'+id_compra;
          break;
        case 3:
          folio = 'Reface00'+id_compra;
          break;
        case 4:
          folio = 'Reface0'+id_compra;
          break;
        case 5:
          folio = 'Reface'+id_compra;
          break;
      }
      sendEmail2(user,carrito_compras,query,folio) 
      return res.json({success:true,message:'Correo enviado'})
      //return res.json({success:true,message:'Encontrado'})
    }
  });
});

router.post('/add_buy', function(req, res, next) {
  var specific_data = req.body;
  console.log('DATOS DE COMPRA')
  console.log(specific_data)
  console.log(specific_data[0].id)
  var query = {
    'id': specific_data[0].id,
    'estado': specific_data[1].estado
  }
  compras_libre.create(query,function (err, compras_libre){
    console.log(query)
    console.log(compras_libre)
    if(err)
      return res.status(500).send('Error en la peticion');
    if(!compras_libre)
      return res.status(404).send({message: 'Ningun registro identificado'});
    else
      return res.status(404).send({message: 'Saved!'});
  });
});


//metodo para actualizar el stock
router.post('/agregar', function(req, res, next) {
  var data = req.body;
  console.log(data)
  console.log(data[0].id)
  var total = data[0].cantidad * data[0].precio
  console.log(total)
  var query = {
    'cantidades': data[0].cantidad,
    'total': total
  }

  carrito_compras.updateOne({'_id': data[0].id}, query)
  .then((producto) => {
    console.log('update')
    console.log(producto)
    return res.json(producto);
  })
  .catch((err) => {
    return res.status(500).send('Error en la peticion');
  });
});

//metodo para actualizar el stock
router.post('/finish', function(req, res, next) {
  var data = req.body;
  console.log(data)
  var query = {
    'total_productos': data[0].total_productos,
    'total_venta':data[0].total_venta,
    'estado':data[0].estado
  }
  console.log(query)
  compras_libre.updateOne({'id': data[0].id}, query)
  .then((producto) => {
    console.log('update')
    console.log(producto)
    return res.json(producto);
  })
  .catch((err) => {
    return res.status(500).send('Error en la peticion');
  });
});

router.post('/borrar_carr', function(req, res, next) {
  var code = req.body;
  //console.log(code[0].id)

  var query = {
    'id_usuario': code.id_usuario,
    'id_producto':code.id_producto
  }
  console.log(query)
  //metodo para buscar el producto
  carrito_compras.remove(query)
  .then((producto) => {
    console.log('borrado')    
    return res.json({success:true,message:producto})
  })
  .catch((err) => {
    console.log(err)
    return res.json({success:false,message:'Error en la peticion'})
  });
});

router.post('/limpiar_car', function(req, res, next) {
  var code = req.body;
  //console.log(code[0].id)

  var query = {
    'id_usuario': code.id_usuario,
    'id_compras': ""
  }
  console.log(query)
  //metodo para buscar el producto
  carrito_compras.remove(query)
  //carrito_compras.find(query)
  .then((producto) => {
    console.log('borrado') 
    console.log(producto)   
    return res.json({success:true,message:producto})
  })
  .catch((err) => {
    console.log(err)
    return res.json({success:false,message:'Error en la peticion'})
  });
});

router.post('/addcar', function(req, res, next) {
  var specific_data = req.body;
  console.log('DATOS DE CARRITO')
  console.log(specific_data)
  var query = {
    'id_compras': "",
    "id_usuario": specific_data.usuario,
    'clave_productos': specific_data.clave_producto,
    'id_producto': specific_data.id,
    'nombre': specific_data.nombre,
    'cantidades': specific_data.cantidades,
    'precio':specific_data.precio,
    'total':specific_data.total,
    'stock':specific_data.stock
  }
  console.log('DATOS Producto')
  console.log(query)
  carrito_compras.create(query,function (err, carrito_compras){
    if(err)
      return res.status(500).send('Error en la peticion');
    if(!carrito_compras){
      return res.json({success:false,message:'no se guardado '})
    }else{
      console.log(carrito_compras)
      return res.json({success:true,message:'guardado ',data:'termino'})
    }
  });
});

router.post('/edit_car', function(req, res, next) {
  var specific_data = req.body;
  console.log('DATOS DE CARRITO')
  console.log(specific_data)
  var query = {
    "id_usuario": specific_data.id_usuario,
    'id_producto': specific_data.id_producto
  }
  var query2 = {
    "cantidades": specific_data.n_cant,
    'total': specific_data.n_precio
  }
  console.log('DATOS')
  console.log(query)
  console.log(query2)
  //return res.json({success:true,message:'Encontro '})
  carrito_compras.updateOne(query, query2,function (err, carrito_compras){
    if(err)
      return res.status(500).send('Error en la peticion');
    if(!carrito_compras){
      return res.json({success:false,message:'No encontro '})
    }else{
      console.log(carrito_compras)
      return res.json({success:true,message:'Modificado'})
    }
  });
});

router.post('/buscar_carrito', function(req, res, next) {
  var specific_data = req.body;
  console.log('DATOS DE CARRITO')
  console.log(specific_data)
  var query = {
    "id_usuario": specific_data.id_usuario,
    'id_producto': specific_data.id_producto,
    'id_compras': specific_data.id_compras
  }
  console.log('DATOS')
  console.log(query)
  //return res.json({success:true,message:'Encontro '})
  carrito_compras.findOne(query,function (err, carrito_compras){
    if(err)
      return res.status(500).send('Error en la peticion');
    if(!carrito_compras){
      return res.json({success:false,message:'No encontro '})
    }else{
      console.log(carrito_compras)
      return res.json({success:true,message:'Encontrado',data:carrito_compras})
    }
  });
});

/*Get last index car*/
router.get('/lastindex', function(req, res, next) {
  console.log('entro al ultimo id')
  compras_libre.find().sort({$natural: -1}).limit(1)
  .then((rawResponse) =>{
    if(!rawResponse){
      return res.status(404).send({message: 'Ningun identificador '});
    }else{
      console.log(rawResponse)
      return res.json({success:true,message:'guardado ',data:rawResponse[0].id});
    }
  })
  .catch((err) => {
    console.log(err)
    return res.json({success:false,message:'Error en la peticion ',data:'no hay'});
    //return res.status(500).send('Error en la peticion');
  });
});


router.post('/listcar2', function(req, res, next) {
  console.log('entro')
  var data = req.body;
  console.log(data)
  var query = {
    'id_compras': "",
    "id_usuario": data.nombre
  }
  console.log(query)
  //return res.json({success:true,message:data.nombre});
  carrito_compras.find(query)
  .then((rawResponse) =>{
    //console.log(rawResponse)
    if(!rawResponse){
      return res.status(404).send({message: 'Ningun identificador '});
    }else{
      console.log(rawResponse)
      return res.json({success:true,message:'encontrado ',data:rawResponse})
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

paypal.configure({
  'mode': 'sandbox', //sandbox or live
  'client_id': 'AcIEZdQ2TaHdTmZSZpVnW-L-3vc_VPC1sJI4c3SQ5LrYdnVZVAiXGyQdcQtWM1slAh03wD-vJFK4d8er',
  'client_secret': 'EN2IazOV7lO4fE5HaR7Yon2QuY-IQjetD1RlT3EXoynyZc24oSbHPqyswJNqdeukB6kk5thOm_VeaMsP'
});

router.get('/pay', function(req, res){
  const create_payment_json = {
    "intent": "sale",
    "payer": {
        "payment_method": "paypal"
    },
    "redirect_urls": {
        "return_url": "http://10.5.0.7:62/replace-sys/imagenes_productos:3000/aplicacion/success",
        "cancel_url": "http://10.5.0.7:62/replace-sys/imagenes_productos:3000/aplicacion/cancel"
    },
    "transactions": [{
        "item_list": {
            "items": [{
                "name": "Red Sox Hat",
                "sku": "001",
                "price": "1",
                "currency": "MX",
                "quantity": "1"
            }]
        },
        "amount": {
            "currency": "MX",
            "total": "1"
        },
        "description": "Hat for the best team ever"
    }]
  };
  console.log(create_payment_json)
  paypal.payment.create(create_payment_json, function (error, payment) {
    if (error) {
        console.log(error)
        throw error;
    } else {
        for(let i = 0;i < payment.links.length;i++){
          if(payment.links[i].rel === 'approval_url'){
            res.redirect(payment.links[i].href);
          }
        }
    }
  });

  

  
});

router.get('/cancel', function(req, res) {
  res.send('Cancelled');
});

router.post('/actualizar_car', function(req, res, next) {
  //console.log('datos:',req.body)
  var datos = req.body['dataCars[]']
  var i = 0
  var a = 0
  var n = 0
  for(i; i< datos.length; n++){
    //i = a
    //console.log('registro: ',datos[i])
    var info = JSON.parse(datos[a]);
    var usuario = req.body['usuario']
    //console.log('separado: ',info)
    var query = {
      "id_usuario": usuario,
      'id_producto': info.ID
    }
    console.log('DATOS')
    console.log(query)
    //return res.json({success:true,message:'Encontro '})
    carrito_compras.findOne(query,function (err, carrito_compras){
      if(err){      
        a++
        i = a
        return res.status(500).send('Error en la peticion');
       
      }
      if(!carrito_compras){
        
        //return res.json({success:false,message:'No encontro '})
        console.log('no encontrado')
        //var specific_data = req.body;
        console.log('DATOS DE CARRITO')
        //console.log(specific_data)
        var info2 = info
        var total = info2.Stock * info2.Precio
        var query2 = {
          'id_compras': "",
          "id_usuario": usuario,
          'clave_productos': info2.Clave,
          'id_producto': info2.ID,
          'nombre': info2.Nombre,
          'cantidades': info.Stock,
          'precio':info2.Precio,
          'total':total,
          'stock':info2.Stock2
        }
        console.log('DATOS Producto')
        console.log(query2)
        carrito_compras2.create(query2,function (err, carrito_compras2){
          if(err)
            console.log('err:', err)
            a++
            i = a
            //return res.status(500).send('Error en la peticion');
          if(!carrito_compras2){
            console.log('no de guardo')
            a++
            i = a
            //return res.json({success:false,message:'no se guardado '})
          }else{
            console.log('Se guardo')
            a++
            i = a
           //return res.json({success:true,message:'guardado ',data:'termino'})
          }
        });
      }else{
        a++
        i = a
        console.log(carrito_compras)
        //return res.json({success:true,message:'Encontrado',data:carrito_compras})
      }
    });
  }
  return res.json({success:true,message:'Se guardaron los datos'});
 
});
 

  
router.get('/success', function(req, res) {
  const payerId = req.query.PayerID;
  const paymentId = req.query.paymentId;
  var dato = res.data
  const execute_payment_json = {
    "payer_id": payerId,
    "transactions": [{
        "amount": {
            "currency": "MX",
            "total": dato[0].total
        }
    }]
  };

  paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
    if (error) {
        console.log(error.response);
        throw error;
    } else {
        console.log(JSON.stringify(payment));
        res.send('Success');
    }
  });

});

router.post('/mybuys', function(req, res, next) {
  console.log('entro')
  var data = req.body;
  console.log(data)
  console.log(data.id)
  compras_libre.find({'datos_personales.email': data.id})
  .then((rawResponse) =>{
    console.log('la raw response fue: ');
    console.log(rawResponse);
    if(!rawResponse){
      return res.status(404).send({message: 'Ningun identificador '});
    }else{
      console.log(rawResponse)
      return res.json({success: true, data: rawResponse});
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

router.post('/registrarimagen', function(req, res, next) {
  console.log('entro')
  var data = req.body;
  console.log(data)
  console.log(data.id)
  compras_libre.find({'datos_personales.email': data.id})
  .then((rawResponse) =>{
    console.log('la raw response fue: ');
    console.log(rawResponse);
    if(!rawResponse){
      return res.status(404).send({message: 'Ningun identificador '});
    }else{
      console.log(rawResponse)
      return res.json({success: true, data: rawResponse});
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

router.post('/image', function(req, res, next) {
  console.log('archivo info')
  let EDFile = req.files
  let datos = req.body
  console.log('datos: ',datos)
  const data = EDFile.uploadedfile.data
  //console.log('archivos: ',data)
  const name = EDFile.uploadedfile.name
  console.log(name)
  var baseURL = path.normalize(__dirname+'/../')+'Files/Oxxo/'
  console.log(baseURL+name)
  var query = {
    'id': datos.id_compra
  }
  console.log('query: ',query)
  let archivo = EDFile.uploadedfile
  console.log('archivo',archivo)

  archivo.mv(baseURL+name, function(err)
    {
      if (err)
      {
        console.log(err)
        console.log("no movio nada");
        return res.json({success:false,message:'No se guardo la imagen'});
      }else{
        compras_libre.findOne(query)
        .then((rawResponse) =>{
          //console.log('la raw response fue: corr');
          //console.log(rawResponse);
          if(!rawResponse){
            return res.json({success:false,message:'No se encontro'});
          }else{
            compras_libre.updateOne(
              query,
              {imagen: name}
            ).then((rawResponse) => {
              console.log(rawResponse)
              return res.json({success:true,message:'Se guardaro la imagen correctamente',data:name});
            })
            .catch((err) => {
              // manejar error
            });
           
          }
        })
        .catch((err) => {
          console.log(err)
          return res.json({success:false,message:'Error en la petición'});
        });
        
      }
      /*compras_libre.findOne(query,function (err, compras_libre){

      })*/
      
    })
  //return res.json({success:true,message:'Se guardaron los datos',data:name});
});

router.post('/imagen',function (request, response) {
  console.log('archivo',request.files)
  return res.json({success:true,message:'Se guardaron los datos',data:name});
})

router.post('/listcar', function(req, res, next) {
  console.log('entro')
  var data = req.body;
  console.log(data)
  console.log(data.id_compras)
  console.log('datos:')
  var query = {
    'id_compras': data.id_compras
  }
  console.log(query)
  carrito_compras.find(query)
  .then((rawResponse) =>{
    console.log(rawResponse)
    if(!rawResponse){
      console.log('error: ',rawResponse)
      return res.status(404).send({success:false, data:null, message: 'Ningun identificador '});
    }else{
      console.log(rawResponse)
      return res.json({success:true, data:rawResponse, message:"Datos encontrados"});
    }
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send({success:false, data:null, message:'Error en la peticion'});
  });
  //return res.json('rawResponse');
});
function sendEmail(to,data,cuenta,id_compra) {

  console.log('entro a email')
  console.log(cuenta)
  var mailOptions = {
    from: "REFACE Pedidos <atencion.clientes@reface.com.mx>",
    to: to,
    subject: "Pedido Realizado",
    html: htmlMessageResetPassword(data,id_compra,cuenta),
  }

  transporter.sendMail(mailOptions, (error, info)=>{
    transporter.close()
    if (error) {
      console.log('error: ',error)
      return res.send(500, err.message);
    }else{
      console.log('info: ',info)
      return res.status(200).jsonp(req.body);
    }
  })
}
function sendEmail2(to,data,cuenta,id_compra) {

  console.log('entro a email')
  console.log(cuenta)
  var mailOptions = {
    from: "REFACE Pedidos <atencion.clientes@reface.com.mx>",
    to: to,
    subject: "Pedido Pagado",
    html: pagado(data,id_compra,cuenta),
  }

  transporter.sendMail(mailOptions, (error, info)=>{
    transporter.close()
    if (error) {
      console.log('error: ',error)
      return res.send(500, err.message);
    }else{
      console.log('info: ',info)
      return res.status(200).jsonp(req.body);
    }
  })
}

function sendEmail3(to,data,cuenta,id_compra) {

  console.log('entro a email')
  console.log(cuenta)
  var mailOptions = {
    from: "REFACE Pedidos <atencion.clientes@reface.com.mx>",
    to: to,
    subject: "Pedido Pagado",
    html: pagado2(data,id_compra,cuenta),
  }

  transporter.sendMail(mailOptions, (error, info)=>{
    transporter.close()
    if (error) {
      console.log('error: ',error)
      return res.send(500, err.message);
    }else{
      console.log('info: ',info)
      return res.status(200).jsonp(req.body);
    }
  })
}

function htmlMessageResetPassword(email,key,cuenta) {
  return "<!DOCTYPE html>\n" +
      "<html>\n" +
      "<head>\n" +
      "\n" +
      "  <meta charset=\"utf-8\">\n" +
      "  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n" +
      "  <title>Password Reset</title>\n" +
      "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
      "  <style type=\"text/css\">\n" +
      "  /**\n" +
      "   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.\n" +
      "   */\n" +
      "  @media screen {\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 400;\n" +
      "      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');\n" +
      "    }\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 700;\n" +
      "      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');\n" +
      "    }\n" +
      "  }\n" +
      "  /**\n" +
      "   * Avoid browser level font resizing.\n" +
      "   * 1. Windows Mobile\n" +
      "   * 2. iOS / OSX\n" +
      "   */\n" +
      "  body,\n" +
      "  table,\n" +
      "  td,\n" +
      "  a {\n" +
      "    -ms-text-size-adjust: 100%; /* 1 */\n" +
      "    -webkit-text-size-adjust: 100%; /* 2 */\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove extra space added to tables and cells in Outlook.\n" +
      "   */\n" +
      "  table,\n" +
      "  td {\n" +
      "    mso-table-rspace: 0pt;\n" +
      "    mso-table-lspace: 0pt;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Better fluid images in Internet Explorer.\n" +
      "   */\n" +
      "  img {\n" +
      "    -ms-interpolation-mode: bicubic;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove blue links for iOS devices.\n" +
      "   */\n" +
      "  a[x-apple-data-detectors] {\n" +
      "    font-family: inherit !important;\n" +
      "    font-size: inherit !important;\n" +
      "    font-weight: inherit !important;\n" +
      "    line-height: inherit !important;\n" +
      "    color: inherit !important;\n" +
      "    text-decoration: none !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Fix centering issues in Android 4.4.\n" +
      "   */\n" +
      "  div[style*=\"margin: 16px 0;\"] {\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  body {\n" +
      "    width: 100% !important;\n" +
      "    height: 100% !important;\n" +
      "    padding: 0 !important;\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Collapse table borders to avoid space between cells.\n" +
      "   */\n" +
      "  table {\n" +
      "    border-collapse: collapse !important;\n" +
      "  }\n" +
      "  a {\n" +
      "    color: #1a82e2;\n" +
      "  }\n" +
      "  p {\n" +
      "    color: #000000;\n" +
      "  }\n" +
      "  img {\n" +
      "    height: auto;\n" +
      "    line-height: 100%;\n" +
      "    text-decoration: none;\n" +
      "    border: 0;\n" +
      "    outline: none;\n" +
      "  }\n" +
      "  </style>\n" +
      "\n" +
      "</head>\n" +
      "<body style=\"background-color: #e9ecef;\">\n" +
      "\n" +
      "  <!-- start preheader -->\n" +
      "  <div class=\"preheader\" style=\"display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;\">\n" +
      "    A preheader is the short summary text that follows the subject line when an email is viewed in the inbox.\n" +
      "  </div>\n" +
      "  <!-- end preheader -->\n" +
      "\n" +
      "  <!-- start body -->\n" +
      "  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" +
      "\n" +
      "    <!-- start logo -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"center\" valign=\"top\" style=\"padding: 36px 24px;\">\n" +
      "                <img src=\"http://10.5.0.7:62/assets/images/logo/logo2.png\" alt=\"Reface\" border=\"0\" width=\"48\" style=\"display: block; width: 48px; max-width: 48px; min-width: 48px;\">\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end logo -->\n" +
      "\n" +
      "    <!-- start hero -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;\">\n" +
      "              <h1 style=\"margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;\">Pedido Realizado</h1>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end hero -->\n" +
      "\n" +
      "    <!-- start copy block -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "\n" +
      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">¡Su pedido se a realizado correctamente!</p>\n" +
      "              <p style=\"margin: 0;\">Favor de hacer el deposito vía Oxxo a los siguientes datos para proceder con el envío:</p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +
      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">Banco: "+cuenta.banco+"</p>\n" +
      "              <p style=\"margin: 0;\">"+cuenta.cuenta+"</p>\n" +
      "              <p style=\"margin: 0;\">Folio de Compra: "+key+"</p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +
      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">Favor de realizar su deposito en un periodo no mayor a 24 horas.</p>\n" +
      "              <p style=\"margin: 0;\">Una vez hecho el pago, subir una foto de su ticket de pago a la siguiente liga:</p>\n" +
      "               <a href=\"http://10.5.0.7:62/mi-cuenta.html\" target=\"blank\" style=\"display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px;  border-radius: 6px;\">Subir imagen</a>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +

      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf\">\n" +
      "              <p style=\"margin: 0;\">Atentamente:<br> Reface</p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end copy block -->\n" +
      "\n" +
      "  </table>\n" +
      "  <!-- end body -->\n" +
      "\n" +
      "</body>\n" +
      "</html>"
}
function pagado(email,key,cuenta) {
  return "<!DOCTYPE html>\n" +
      "<html>\n" +
      "<head>\n" +
      "\n" +
      "  <meta charset=\"utf-8\">\n" +
      "  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n" +
      "  <title>Password Reset</title>\n" +
      "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
      "  <style type=\"text/css\">\n" +
      "  /**\n" +
      "   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.\n" +
      "   */\n" +
      "  @media screen {\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 400;\n" +
      "      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');\n" +
      "    }\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 700;\n" +
      "      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');\n" +
      "    }\n" +
      "  }\n" +
      "  /**\n" +
      "   * Avoid browser level font resizing.\n" +
      "   * 1. Windows Mobile\n" +
      "   * 2. iOS / OSX\n" +
      "   */\n" +
      "  body,\n" +
      "  table,\n" +
      "  td,\n" +
      "  a {\n" +
      "    -ms-text-size-adjust: 100%; /* 1 */\n" +
      "    -webkit-text-size-adjust: 100%; /* 2 */\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove extra space added to tables and cells in Outlook.\n" +
      "   */\n" +
      "  table,\n" +
      "  td {\n" +
      "    mso-table-rspace: 0pt;\n" +
      "    mso-table-lspace: 0pt;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Better fluid images in Internet Explorer.\n" +
      "   */\n" +
      "  img {\n" +
      "    -ms-interpolation-mode: bicubic;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove blue links for iOS devices.\n" +
      "   */\n" +
      "  a[x-apple-data-detectors] {\n" +
      "    font-family: inherit !important;\n" +
      "    font-size: inherit !important;\n" +
      "    font-weight: inherit !important;\n" +
      "    line-height: inherit !important;\n" +
      "    color: inherit !important;\n" +
      "    text-decoration: none !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Fix centering issues in Android 4.4.\n" +
      "   */\n" +
      "  div[style*=\"margin: 16px 0;\"] {\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  body {\n" +
      "    width: 100% !important;\n" +
      "    height: 100% !important;\n" +
      "    padding: 0 !important;\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Collapse table borders to avoid space between cells.\n" +
      "   */\n" +
      "  table {\n" +
      "    border-collapse: collapse !important;\n" +
      "  }\n" +
      "  a {\n" +
      "    color: #1a82e2;\n" +
      "  }\n" +
      "  p {\n" +
      "    color: #000000;\n" +
      "  }\n" +
      "  img {\n" +
      "    height: auto;\n" +
      "    line-height: 100%;\n" +
      "    text-decoration: none;\n" +
      "    border: 0;\n" +
      "    outline: none;\n" +
      "  }\n" +
      "  </style>\n" +
      "\n" +
      "</head>\n" +
      "<body style=\"background-color: #e9ecef;\">\n" +
      "\n" +
      "  <!-- start preheader -->\n" +
      "  <div class=\"preheader\" style=\"display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;\">\n" +
      "    A preheader is the short summary text that follows the subject line when an email is viewed in the inbox.\n" +
      "  </div>\n" +
      "  <!-- end preheader -->\n" +
      "\n" +
      "  <!-- start body -->\n" +
      "  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" +
      "\n" +
      "    <!-- start logo -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"center\" valign=\"top\" style=\"padding: 36px 24px;\">\n" +
      "                <img src=\"http://tiendavirtual.dyndns.org:62/assets/images/logo/logo2.png\" alt=\"Reface\" border=\"0\" width=\"48\" style=\"display: block; width: 48px; max-width: 130px; min-width: 48px;\">\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end logo -->\n" +
      "\n" +
      "    <!-- start hero -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;\">\n" +
      "              <h1 style=\"margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;\">Pedido Realizado</h1>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end hero -->\n" +
      "\n" +
      "    <!-- start copy block -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "\n" +
      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">¡Se realizo el pago de su compra correctamente!</p>\n" +
      "              <p style=\"margin: 0;\">No. de compra: "+key+" </p>\n" +
      "              <p style=\"margin: 0;\">El envió de su compra se realizara en los proximos días</p>\n" +
      "              <p style=\"margin: 0;\">Puede ver el siguimiento en el siguiente enlace <a href=\"http://tiendavirtual.dyndns.org:62/mi-cuenta.html\">Mis Compras</a></p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +
      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">Puede hacer el seguimiento se su compra en la siguiente liga</p>\n" +
      "               <a href=\"http://10.5.0.7:62/mi-cuenta.html\" target=\"blank\" style=\"display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px;  border-radius: 6px;\">Subir imagen</a>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +

      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf\">\n" +
      "              <p style=\"margin: 0;\">Atentamente:<br> Reface</p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end copy block -->\n" +
      "\n" +
      "  </table>\n" +
      "  <!-- end body -->\n" +
      "\n" +
      "</body>\n" +
      "</html>"
}

function pagado2(email,key,cuenta) {
  return "<!DOCTYPE html>\n" +
      "<html>\n" +
      "<head>\n" +
      "\n" +
      "  <meta charset=\"utf-8\">\n" +
      "  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n" +
      "  <title>Password Reset</title>\n" +
      "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
      "  <style type=\"text/css\">\n" +
      "  /**\n" +
      "   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.\n" +
      "   */\n" +
      "  @media screen {\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 400;\n" +
      "      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');\n" +
      "    }\n" +
      "    @font-face {\n" +
      "      font-family: 'Source Sans Pro';\n" +
      "      font-style: normal;\n" +
      "      font-weight: 700;\n" +
      "      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');\n" +
      "    }\n" +
      "  }\n" +
      "  /**\n" +
      "   * Avoid browser level font resizing.\n" +
      "   * 1. Windows Mobile\n" +
      "   * 2. iOS / OSX\n" +
      "   */\n" +
      "  body,\n" +
      "  table,\n" +
      "  td,\n" +
      "  a {\n" +
      "    -ms-text-size-adjust: 100%; /* 1 */\n" +
      "    -webkit-text-size-adjust: 100%; /* 2 */\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove extra space added to tables and cells in Outlook.\n" +
      "   */\n" +
      "  table,\n" +
      "  td {\n" +
      "    mso-table-rspace: 0pt;\n" +
      "    mso-table-lspace: 0pt;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Better fluid images in Internet Explorer.\n" +
      "   */\n" +
      "  img {\n" +
      "    -ms-interpolation-mode: bicubic;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Remove blue links for iOS devices.\n" +
      "   */\n" +
      "  a[x-apple-data-detectors] {\n" +
      "    font-family: inherit !important;\n" +
      "    font-size: inherit !important;\n" +
      "    font-weight: inherit !important;\n" +
      "    line-height: inherit !important;\n" +
      "    color: inherit !important;\n" +
      "    text-decoration: none !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Fix centering issues in Android 4.4.\n" +
      "   */\n" +
      "  div[style*=\"margin: 16px 0;\"] {\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  body {\n" +
      "    width: 100% !important;\n" +
      "    height: 100% !important;\n" +
      "    padding: 0 !important;\n" +
      "    margin: 0 !important;\n" +
      "  }\n" +
      "  /**\n" +
      "   * Collapse table borders to avoid space between cells.\n" +
      "   */\n" +
      "  table {\n" +
      "    border-collapse: collapse !important;\n" +
      "  }\n" +
      "  a {\n" +
      "    color: #1a82e2;\n" +
      "  }\n" +
      "  p {\n" +
      "    color: #000000;\n" +
      "  }\n" +
      "  img {\n" +
      "    height: auto;\n" +
      "    line-height: 100%;\n" +
      "    text-decoration: none;\n" +
      "    border: 0;\n" +
      "    outline: none;\n" +
      "  }\n" +
      "  </style>\n" +
      "\n" +
      "</head>\n" +
      "<body style=\"background-color: #e9ecef;\">\n" +
      "\n" +
      "  <!-- start preheader -->\n" +
      "  <div class=\"preheader\" style=\"display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;\">\n" +
      "    A preheader is the short summary text that follows the subject line when an email is viewed in the inbox.\n" +
      "  </div>\n" +
      "  <!-- end preheader -->\n" +
      "\n" +
      "  <!-- start body -->\n" +
      "  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" +
      "\n" +
      "    <!-- start logo -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"center\" valign=\"top\" style=\"padding: 36px 24px;\">\n" +
      "                <img src=\"http://tiendavirtual.dyndns.org:62/assets/images/logo/logo2.png\" alt=\"Reface\" border=\"0\" width=\"48\" style=\"display: block; width: 48px; max-width: 48px; min-width: 130px;\">\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end logo -->\n" +
      "\n" +
      "    <!-- start hero -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;\">\n" +
      "              <h1 style=\"margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;\">Pedido Realizado</h1>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end hero -->\n" +
      "\n" +
      "    <!-- start copy block -->\n" +
      "    <tr>\n" +
      "      <td align=\"center\" bgcolor=\"#e9ecef\">\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">\n" +
      "        <tr>\n" +
      "        <td align=\"center\" valign=\"top\" width=\"600\">\n" +
      "        <![endif]-->\n" +
      "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n" +
      "\n" +
      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">¡Se realizo el pedido de su compra correctamente!</p>\n" +
      "              <p style=\"margin: 0;\">No. de pedido: "+key+" </p>\n" +
      "              <p style=\"margin: 0;\">Por Favor realice su pago a traves de cualquier tiendo Oxxo con los siguientes datos: </p>\n" +
      "              <p style=\"margin: 0;\">Banco: "+cuenta.banco+"</p>\n" +
      "              <p style=\"margin: 0;\">No. de cuenta: "+cuenta.cuenta+"</p>\n" +     
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +
      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">Una vez realizado el pago favor de subir la foto de su recibo en un tiempo no mayor a 24 hrs en el siguiente enlace <a href=\"http://tiendavirtual.dyndns.org:62/mi-cuenta.html\">Mis Compras</a></p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +

      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n" +
      "              <p style=\"margin: 0;\">¡Gracias por su compra!</p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +

      "          <!-- start copy -->\n" +
      "          <tr>\n" +
      "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf\">\n" +
      "              <p style=\"margin: 0;\">Atentamente:<br> Reface</p>\n" +
      "            </td>\n" +
      "          </tr>\n" +
      "          <!-- end copy -->\n" +
      "\n" +
      "        </table>\n" +
      "        <!--[if (gte mso 9)|(IE)]>\n" +
      "        </td>\n" +
      "        </tr>\n" +
      "        </table>\n" +
      "        <![endif]-->\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "    <!-- end copy block -->\n" +
      "\n" +
      "  </table>\n" +
      "  <!-- end body -->\n" +
      "\n" +
      "</body>\n" +
      "</html>"
}

router.post('/add_cotizacion', function(req, res, next) {

  var specific_data = req.body;

  console.log('Datos a cotizar');
  console.log(specific_data);
  var query = {
    'marca': specific_data.marca,
    'modelo': specific_data.modelo,
    'año': specific_data.año,
    'motor': specific_data.motor,
    'numpart': specific_data.numpart,
    'pieza': specific_data.pieza,
    'nombre':specific_data.nombre,
    'id_usuario': specific_data.id_usuario   
  }

  console.log("query para la busqueda");
  console.log(query);

  //metodo para buscar el usuario
  cotizacion.create(query,function (err, cotizacion){
    if(err){
      console.log(err)
      return res.status(500).send('Error en la peticion');
    }
    if(!cotizacion){
      console.log(cotizacion)
      return res.status(404).send({success:false, message: 'No se guardo'});
    }else{
      console.log(cotizacion)
      return res.send({success:true,message: 'Guardado'});
    }
  });
});

router.post('/list-carrito', function(req, res, next) {
  var datos = req.body;
  console.log(datos.email)

  compras_libre.aggregate([
      {
        $lookup:{
          from: "carritocompras",
          localField:"id",
          foreignField:"id_compras", 
          as: "comprado"
        }
      },
      {$match: {       
          "datos_personales.email": {$eq:datos.email}
          }
      }
      /*{$group : 
        { 
            _id : "$carritocompras.id",
            comprado :"$comprado"
            
        }
      } */
    ],function (err, compras_libre){
        console.log(compras_libre)
        if(err){
          console.log(err)
          return res.status(500).send({success:false, message:'Error en la peticion'});
        }
        if(!compras_libre){
          return res.status(404).send({success:false, message: 'Ningun registro identificado'});
        }
        console.log(compras_libre)
        return res.json({success:true, message:'Datos encontrados',data:compras_libre});
  });
});


router.post('/costo_envio', function(req, res, next) {
  var datos = req.body;
  var productos = JSON.parse(datos.lista)
  //console.log('fecha: ',fecha)
  console.log('datos: ',productos[0])
  console.log('datos: ',productos[0].id_producto)
  var nombre =  ('Refaccionaria Ceballos S. de R.L DE C.V.').toUpperCase()
  //console.log('datos: ',datos['info[domicilio][calle]'])
  //var f = Date()
  //console.log(direccion)
  var direccion = removeAccents('CALLE '+datos['info[domicilio][calle]'].toUpperCase()+' # '+datos['info[domicilio][num_interior]']);
  //console.log(direccion)
  //var fecha = new Date(f.getFullYear(),f.getMonth() +1,f.getDate(),f.getHours(),f.getMinutes(),f.getSeconds())
  var fecha = new Date().toISOString()
  //console.log('datos: ',datos.datos_personales[telefono])
  var obj = {
    'req:ShipmentRequest': {
          '@xmlns:req': 'http://www.dhl.com', 
          '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance', 
          '@xsi:schemaLocation': 'http://www.dhl.com ship-val-global-req.xsd', 
          '@schemaVersion': '6.2' 
    }
  };
  var xml = builder.create(obj,{version: '1.0', encoding: 'UTF-8'})
    .ele('Request')
      .ele('ServiceHeader')
        .ele('MessageTime', fecha).up() 
        .ele('MessageReference','0000000000000000000000000001').up()  
        .ele('SiteID','v62_HsZ1Ce8KXM').up()  
        .ele('Password','APFyPjt8sh').up()  
      .up()
      .ele('MetaData')
        .ele('SoftwareName', 'REFACE WEB').up() 
        .ele('SoftwareVersion','RWEB3.2').up()       
      .up()
    .up()
    .ele('RegionCode', 'AM').up() 
    .ele('RequestedPickupTime', 'Y').up() 
    .ele('NewShipper', 'Y').up() 
    .ele('LanguageCode', 'en').up() 
    .ele('PiecesEnabled', 'Y').up()
    .ele('Billing')
      .ele('ShipperAccountNumber', '980325209').up() 
      .ele('ShippingPaymentType','S').up()       
    .up()
    .ele('Consignee')
      .ele('CompanyName',nombre).up() 
      .ele('AddressLine',direccion).up()       
      .ele('AddressLine',removeAccents(datos['info[domicilio][colonia]'].toUpperCase())).up()  
      .ele('AddressLine',removeAccents(datos['info[domicilio][estado]'].toUpperCase())).up()   
      .ele('City',removeAccents(datos['info[domicilio][municipio]'].toUpperCase())).up()       
      .ele('PostalCode',datos['info[domicilio][codigo_postal]']).up()       
      .ele('CountryCode','MX').up()
      .ele('CountryName',removeAccents(datos['info[domicilio][pais]']).toUpperCase()).up()    
      .ele('Contact')
        .ele('PersonName',removeAccents((datos['info[datos_personales][nombres]']+' '+datos['info[datos_personales][apellidos]'])).toUpperCase()).up()
        .ele('PhoneNumber',datos['info[datos_personales][telefono]']).up()
        .ele('Email',datos['info[datos_personales][email]']).up()
      .up()   
    .up() 
    .ele('ShipmentDetails')
      .ele('NumberOfPieces',1).up()
      .ele('Pieces')
        .ele('Piece')
          .ele('PieceID',1).up()
          .ele('PackageType','JJ').up()
          .ele('Weight',0.200).up()
          .ele('DimWeight',0.0).up()
          .ele('Width',1).up()
          .ele('Height',1).up()
          .ele('Depth',1).up()
        .up()
      .up()
      .ele('Weight',0.200).up()
      .ele('WeightUnit','K').up()
      .ele('GlobalProductCode','N').up()
      .ele('LocalProductCode','N').up()
      .ele('Date',fecha.substring(0,10)).up()
      .ele('Contents',productos[0].nombre.split(" ")[0]).up()
      .ele('DoorTo','DD').up()
      .ele('DimensionUnit','C').up()
      .ele('PackageType','CP').up()
      .ele('IsDutiable','N').up()
      .ele('CurrencyCode','MXN').up()
    .up()
    .ele('Shipper')
      .ele('ShipperID',1).up()
      .ele('CompanyName',nombre).up()
      .ele('AddressLine','AV ACUEDUCTO NO. 1969').up()
      .ele('AddressLine','COL. MARIANO MATAMOROS').up()
      .ele('AddressLine','MORELIA').up()
      .ele('City','MORELIA').up()
      .ele('PostalCode','58240').up()
      .ele('CountryCode','MX').up()
      .ele('CountryName','MEXICO').up()
      .ele('Contact')
        .ele('PersonName','REFACCIONARIA CEBALLOS').up()
        .ele('PhoneNumber','4435064200').up()
        .ele('Email','refaccionariaceballos@gmail.com').up()
      .up()
    .up() 
    .ele('EProcShip','N').up()
    .ele('LabelImageFormat','PDF').up()
    .ele('RequestArchiveDoc','Y').up()
    .ele('NumberOfArchiveDoc',1).up()
  .end({ pretty: true});
 
  console.log(xml);
   /**
    url:"http://192.168.1.148",
    port: 9000,
    method:"POST",
    headers:{
        'Content-Type': 'application/xml',
    },
     body: abc

     json: { key: 'value' }
    */
  request.post( 'http://xmlpitest-ea.dhl.com/XMLShippingServlet', { 
      headers:{
        'Content-Type': 'application/xml',
      },
      body:xml
    }, 
    function (error, response, body) { 
      //console.log(response)
      if (!error && response.statusCode == 200) { 
        console.log('xml: ') 
        console.log(body) 
        //return res.status(200).send({success:false, message: 'No se genero' , data:null});
      }else{//falta generar el pdf de la guía 
        console.log("error: ",error)
        fs.writeFile('pdf_dhl.pdf', stringToDecode, 'base64', error => {
          if (error) {
              throw error;
          } else {
              console.log('base64 saved!');
          }
        });
        //return res.status(200).send({success:true, message: 'Se genero' , data:xml});
      }
    } 
  );
  
  

});

router.post('/envio', function(req, res, next) {
  var datos = req.body;
  var productos = JSON.parse(datos.lista)
  //console.log('fecha: ',fecha)
  console.log('datos: ',productos[0])
  console.log('datos: ',productos[0].id_producto)
  var nombre =  ('Refaccionaria Ceballos S. de R.L DE C.V.').toUpperCase()
  //console.log('datos: ',datos['info[domicilio][calle]'])
  //var f = Date()
  //console.log(direccion)
  var direccion = removeAccents('CALLE '+datos['info[domicilio][calle]'].toUpperCase()+' # '+datos['info[domicilio][num_interior]']);
  //console.log(direccion)
  var f = new Date()
  var mes = (f.getMonth()+1)
  var dia = f.getDate()
  if(String(mes).length == 1){
    mes = "0"+mes
  }
  if(String(dia).length == 1){
    dia = "0"+dia
  }
  console.log('mes: ',mes)
  var fecha = f.getFullYear()+"-"+mes+"-"+dia
  var fecha2 = new Date().toISOString()
  var readyTime = "PT"+f.getHours()+"H"+f.getMinutes()+"M"
  //console.log('datos: ',datos.datos_personales[telefono])
  var obj = {
    'p:DCTRequest': {
          '@xmlns:p': 'http://www.dhl.com', 
          '@xmlns:p1': 'http://www.dhl.com/datatypes', 
          '@xmlns:p2': 'http://www.dhl.com/DCTRequestdatatypes', 
          '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
          '@xsi:schemaLocation': 'http://www.dhl.com DCT-req.xsd',
          '@schemaVersion': '2.0' 
    }
  };
  var xml = builder.create(obj,{version: '1.0', encoding: 'UTF-8'})
    .ele('GetQuote')
      .ele('Request')
        .ele('ServiceHeader')
          .ele('MessageTime', fecha2).up() 
          .ele('MessageReference','0000000000000000000000000001').up()  
          .ele('SiteID','v62_HsZ1Ce8KXM').up()  
          .ele('Password','APFyPjt8sh').up()  
        .up()
        .ele('MetaData')
          .ele('SoftwareName', 'REFACE WEB').up() 
          .ele('SoftwareVersion','RWEB3.2').up()       
        .up()
      .up()
      .ele('From')
        .ele('CountryCode', 'MX').up() 
        .ele('Postalcode','58240').up()       
      .up()
      .ele('BkgDetails')
        .ele('PaymentCountryCode', 'MX').up() 
        .ele('Date',fecha).up()       
        .ele('ReadyTime',readyTime).up()       
        .ele('ReadyTimeGMTOffset','+01:00').up()       
        .ele('DimensionUnit','CM').up()     
        .ele('WeightUnit','KG').up()     
        .ele('Pieces')
          .ele('Piece')
            .ele('PieceID',1).up()
            .ele('Height',1).up()
            .ele('Depth',1).up()
            .ele('Width',1).up()
            .ele('Weight',0.200).up()
          .up()     
        .up()
        .ele('PaymentAccountNumber', '980325209').up() 
        .ele('IsDutiable', 'N').up() 
        .ele('NetworkTypeCode','AL').up()       
        .ele('QtdShp')
          .ele('GlobalProductCode','N').up()
          .ele('LocalProductCode','N').up()         
        .up()       
      .up()
      .ele('To')
        .ele('CountryCode','MX').up()
        .ele('Postalcode',datos['info[domicilio][codigo_postal]']).up()         
      .up()  
    .up()
  .end({ pretty: true});
 
  console.log(xml);
   /**
    url:"http://192.168.1.148",
    port: 9000,
    method:"POST",
    headers:{
        'Content-Type': 'application/xml',
    },
     body: abc

     json: { key: 'value' }
    */
  request.post( 'http://xmlpitest-ea.dhl.com/XMLShippingServlet', { 
      headers:{
        'Content-Type': 'application/xml',
      },
      body:xml
    }, 
    function (error, response, body) { 
      //console.log(response)
      if (!error && response.statusCode == 200) { 
        console.log('xml: ') 
        console.log(body) 
        /*route1 = path.normalize(__dirname + "../../../../../replace-sys/admin-backend/Files/DHL/Envios/")
        console.log(route1)
        var options = {
          object: false,
          reversible: false,
          coerce: false,
          sanitize: true,
          trim: true,
          arrayNotation: false,
          alternateTextNode: false
        };
        //var parser = new DomParser();
        var json = parser.toJson(body,options);*/
        //var dom = parser.parseFromString(body);
        /*var doc = new DOMParser().parseFromString(
          '<xml xmlns="a" xmlns:c="./lite">\n'+
              '\t<child>test</child>\n'+
              '\t<child></child>\n'+
              '\t<child/>\n'+
          '</xml>'
          ,'text/xml');
        doc.documentElement.setAttribute('x','y');
        doc.documentElement.setAttributeNS('./lite','c:x','y2');
        var nsAttr = doc.documentElement.getAttributeNS('./lite','x')
        console.info(nsAttr)
        console.info(doc.documentElement)
        */
        //console.log("to json -> %s", json);
        //console.log("to json -> %s", JSON.stringify(json));
        //console.log("to json -> %s", JSON.parse(json));
        //console.log("to json -> %s", JSON.parse(json).Response);


       /*const xml = body
        const doc = new dom().parseFromString(xml);
        const select = xpath.useNamespaces({
          'bpsdm': 'http://schemas.brother.info/mfc/mailreports/1.00'
        });

        const sn = select('string(//bpsdm:SerialNumber)', doc);
        console.log('Serial Number:', sn);*/
  // imprime:
  // E75337M5N131720

        /*fs.writeFile('pdf_dhl.pdf', stringToDecode, 'base64', error => {
          if (error) {
              throw error;
          } else {
              console.log('base64 saved!');
          }
        });*/
        //return res.status(200).send({success:false, message: 'No se genero' , data:null});
      }else{
        console.log("error: ",error)
       
        //return res.status(200).send({success:true, message: 'Se genero' , data:xml});
      }
    } 
  );
  
  

});

router.post('/tracking', function(req, res, next) {
  var datos = req.body;
  //var productos = JSON.parse(datos)
  console.log('datos: ',datos)
  //console.log('datos: ',productos[0])
  //console.log('datos: ',productos[0].id_producto)
  var nombre =  ('Refaccionaria Ceballos S. de R.L DE C.V.').toUpperCase()
  //console.log('datos: ',datos['info[domicilio][calle]'])
  //var f = Date()
  //console.log(direccion)
  //var direccion = removeAccents('CALLE '+datos['info[domicilio][calle]'].toUpperCase()+' # '+datos['info[domicilio][num_interior]']);
  //console.log(direccion)
  var f = new Date()
  var mes = (f.getMonth()+1)
  var dia = f.getDate()
  if(String(mes).length == 1){
    mes = "0"+mes
  }
  if(String(dia).length == 1){
    dia = "0"+dia
  }
  console.log('mes: ',mes)
  //var fecha = f.getFullYear()+"-"+mes+"-"+dia
  var fecha2 = new Date().toISOString()
  var readyTime = "PT"+f.getHours()+"H"+f.getMinutes()+"M"
  ////console.log('datos: ',datos.datos_personales[telefono])
  var obj = {
    'req:KnownTrackingRequest': {
          '@xmlns:req': 'http://www.dhl.com', 
          '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
          '@xsi:schemaLocation': 'http://www.dhl.com TrackingRequestKnown.xsd', 
          '@schemaVersion': '1.0' 
    }
  };
  var xml = builder.create(obj,{version: '1.0', encoding: 'UTF-8'})   
    .ele('Request')
      .ele('ServiceHeader')
        .ele('MessageTime', fecha2).up() 
        .ele('MessageReference','0000000000000000000000000001').up()  
        .ele('SiteID','v62_HsZ1Ce8KXM').up()  
        .ele('Password','APFyPjt8sh').up()  
      .up()
    .up()
    .ele('LanguageCode', 'EN').up() 
    .ele('AWBNumber','7813645022').up()    
    .ele('LevelOfDetails','ALL_CHECK_POINTS').up()        
  .end({ pretty: true});
 
  console.log(xml);
   /**
    url:"http://192.168.1.148",
    port: 9000,
    method:"POST",
    headers:{
        'Content-Type': 'application/xml',
    },
     body: abc

     json: { key: 'value' }
    */
  request.post( 'http://xmlpitest-ea.dhl.com/XMLShippingServlet', { 
      headers:{
        'Content-Type': 'application/xml',
      },
      body:xml
    }, 
    function (error, response, body) { 
      //console.log(response)
      if (!error && response.statusCode == 200) { 
        console.log('xml: ') 
        console.log(body) 
        /*route1 = path.normalize(__dirname + "../../../../../replace-sys/admin-backend/Files/DHL/Envios/")
        console.log(route1)
        var options = {
          object: false,
          reversible: false,
          coerce: false,
          sanitize: true,
          trim: true,
          arrayNotation: false,
          alternateTextNode: false
        };
        //var parser = new DomParser();
        var json = parser.toJson(body,options);*/
        //var dom = parser.parseFromString(body);
        /*var doc = new DOMParser().parseFromString(
          '<xml xmlns="a" xmlns:c="./lite">\n'+
              '\t<child>test</child>\n'+
              '\t<child></child>\n'+
              '\t<child/>\n'+
          '</xml>'
          ,'text/xml');
        doc.documentElement.setAttribute('x','y');
        doc.documentElement.setAttributeNS('./lite','c:x','y2');
        var nsAttr = doc.documentElement.getAttributeNS('./lite','x')
        console.info(nsAttr)
        console.info(doc.documentElement)
        */
        //console.log("to json -> %s", json);
        //console.log("to json -> %s", JSON.stringify(json));
        //console.log("to json -> %s", JSON.parse(json));
        //console.log("to json -> %s", JSON.parse(json).Response);


       /*const xml = body
        const doc = new dom().parseFromString(xml);
        const select = xpath.useNamespaces({
          'bpsdm': 'http://schemas.brother.info/mfc/mailreports/1.00'
        });

        const sn = select('string(//bpsdm:SerialNumber)', doc);
        console.log('Serial Number:', sn);*/
  // imprime:
  // E75337M5N131720

        /*fs.writeFile('pdf_dhl.pdf', stringToDecode, 'base64', error => {
          if (error) {
              throw error;
          } else {
              console.log('base64 saved!');
          }
        });*/
        //return res.status(200).send({success:false, message: 'No se genero' , data:null});
      }else{
        console.log("error: ",error)
       
        //return res.status(200).send({success:true, message: 'Se genero' , data:xml});
      }
    } 
  );
  
  

});

router.post('/pickup', function(req, res, next) {
  var datos = req.body;
  //var productos = JSON.parse(datos)
  console.log('datos: ',datos)
  //console.log('datos: ',productos[0])
  //console.log('datos: ',productos[0].id_producto)
  var nombre =  ('Refaccionaria Ceballos S. de R.L DE C.V.').toUpperCase()
  //console.log('datos: ',datos['info[domicilio][calle]'])
  //var f = Date()
  //console.log(direccion)
  //var direccion = removeAccents('CALLE '+datos['info[domicilio][calle]'].toUpperCase()+' # '+datos['info[domicilio][num_interior]']);
  //console.log(direccion)
  var f = new Date()
  var mes = (f.getMonth()+1)
  var dia = f.getDate()
  if(String(mes).length == 1){
    mes = "0"+mes
  }
  if(String(dia).length == 1){
    dia = "0"+dia
  }
  console.log('mes: ',mes)
  //var fecha = f.getFullYear()+"-"+mes+"-"+dia
  var fecha2 = new Date().toISOString()
  var readyTime = "PT"+f.getHours()+"H"+f.getMinutes()+"M"
  ////console.log('datos: ',datos.datos_personales[telefono])
  var obj = {
    'req:BookPURequest': {
          '@xmlns:req': 'http://www.dhl.com', 
          '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
          '@xsi:schemaLocation': 'http://www.dhl.com book-pickup-global-req.xsd', 
          '@schemaVersion': '1.0' 
    }
  };
  var xml = builder.create(obj,{version: '1.0', encoding: 'UTF-8'})   
    .ele('Request')
      .ele('ServiceHeader')
        .ele('MessageTime', fecha2).up() 
        .ele('MessageReference','').up()  
        .ele('SiteID','v62_HsZ1Ce8KXM').up()  
        .ele('Password','APFyPjt8sh').up()  
      .up()
      .ele('MetaData')
        .ele('SoftwareName', 'REFACE WEB').up() 
        .ele('SoftwareVersion','RWEB3.2').up()       
      .up()
    .up()
    .ele('RegionCode', 'AM').up() 
    .ele('Requestor')
      .ele('AccountType','D').up()        
      .ele('AccountNumber','980325209').up()        
      .ele('RequestorContact')
        .ele('PersonName','D').up()    
        .ele('Phone','980325209').up()  
        .ele('AddressLine','AV ACUEDUCTO NO. 1969').up()
        .ele('AddressLine','COL. MARIANO MATAMOROS').up()
        .ele('AddressLine','MORELIA').up()
        .ele('City','MORELIA').up()
        .ele('PostalCode','58240').up()  
      .up()        
    .up()    
    .ele('LevelOfDetails','ALL_CHECK_POINTS').up()        
  .end({ pretty: true});
 
  console.log(xml);
   /**
    url:"http://192.168.1.148",
    port: 9000,
    method:"POST",
    headers:{
        'Content-Type': 'application/xml',
    },
     body: abc

     json: { key: 'value' }
    */
  request.post( 'http://xmlpitest-ea.dhl.com/XMLShippingServlet', { 
      headers:{
        'Content-Type': 'application/xml',
      },
      body:xml
    }, 
    function (error, response, body) { 
      //console.log(response)
      if (!error && response.statusCode == 200) { 
        console.log('xml: ') 
        console.log(body) 
        /*route1 = path.normalize(__dirname + "../../../../../replace-sys/admin-backend/Files/DHL/Envios/")
        console.log(route1)
        var options = {
          object: false,
          reversible: false,
          coerce: false,
          sanitize: true,
          trim: true,
          arrayNotation: false,
          alternateTextNode: false
        };
        //var parser = new DomParser();
        var json = parser.toJson(body,options);*/
        //var dom = parser.parseFromString(body);
        /*var doc = new DOMParser().parseFromString(
          '<xml xmlns="a" xmlns:c="./lite">\n'+
              '\t<child>test</child>\n'+
              '\t<child></child>\n'+
              '\t<child/>\n'+
          '</xml>'
          ,'text/xml');
        doc.documentElement.setAttribute('x','y');
        doc.documentElement.setAttributeNS('./lite','c:x','y2');
        var nsAttr = doc.documentElement.getAttributeNS('./lite','x')
        console.info(nsAttr)
        console.info(doc.documentElement)
        */
        //console.log("to json -> %s", json);
        //console.log("to json -> %s", JSON.stringify(json));
        //console.log("to json -> %s", JSON.parse(json));
        //console.log("to json -> %s", JSON.parse(json).Response);


       /*const xml = body
        const doc = new dom().parseFromString(xml);
        const select = xpath.useNamespaces({
          'bpsdm': 'http://schemas.brother.info/mfc/mailreports/1.00'
        });

        const sn = select('string(//bpsdm:SerialNumber)', doc);
        console.log('Serial Number:', sn);*/
  // imprime:
  // E75337M5N131720

        /*fs.writeFile('pdf_dhl.pdf', stringToDecode, 'base64', error => {
          if (error) {
              throw error;
          } else {
              console.log('base64 saved!');
          }
        });*/
        //return res.status(200).send({success:false, message: 'No se genero' , data:null});
      }else{
        console.log("error: ",error)
       
        //return res.status(200).send({success:true, message: 'Se genero' , data:xml});
      }
    } 
  );
  
  

});

const removeAccents = (str) => {
  return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
} 

module.exports = router;

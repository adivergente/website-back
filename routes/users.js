require('dotenv').config()
const express = require('express')
const router = express.Router()
const Usuario = require('../models/usuarios')
const dayjs = require('dayjs')
const shajs = require('sha.js')
const jwt = require('jsonwebtoken')
const jwtKey = process.env.JWT_SECRET || 'secret'
const isAuthenticated = require('../middlewares/authMiddleware')
const { sendResetPassword, sendVerificationEmail } = require('../utils/mailing')

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.send('respond with a resource');
});

router.post('/email-available', (req, res, next) => {
	const { email } = req.body
	console.log('email', email)
	Usuario.findOne({ 'datos_personales.email': email }, (err, usuario) => {
		if(err) return res.status(500).send('Error en la peticion');
		return !usuario
			? res.json({ success:true, message:'Disponible', data: true })
			: res.json({ success:true, message:'No disponible', data: false })
	})
})

/* POST user by id */
router.post('/login', (req, res, next) => {
  // TODO: si no se tiene verified at enviar false con data que diga que no se ha validado, silo tiene seguir flujo normal
  const datos = req.body;
  query = { $and: [
        { "datos_personales.email": datos.email },
        { "datos_personales.password": shajs('sha256').update(datos.password).digest('hex') }
      ]
    }
  Usuario.findOne(query, (err, user) => {
    if (err) {
      // console.log('error:', err)
      return res.status(409).json({ success: false, message: err, data: [] })
    }

    if (user && !user.email_verified_at) {
      return res.json({ success: false, message: 'Se requiere validación', data: { type: 'validation' } })
    } else {
      const token = createJwt(user)
      return user.status.toLowerCase() === 'activo'
        ? res.json({ success: true, message: 'Bienvenido', data: token })
        : res.json({ success: false, message: 'Su cuenta se encuentra inactiva, contacte con soporte', data: { type: 'inactive' } })
    }

    return res.json({ success: false, message: 'Contraseña o usuario incorrectos', data: { type: 'credentials' } })
  })
});

/* create new usuario */
router.post('/register', (req, res, next) => {
  const data = req.body;
  const pass = shajs('sha256').update(data.password).digest('hex')
  const user = {
    'nombres':          data.nombre,
    'apellidos':        data.apellido,
    'username':         "",
    'email':            data.email.toLowerCase(),
    'password':         pass,
    'telefono':         data.telefono
  }
  const query = {
    'id': data.email,
    'datos_personales': user
  }
  
  Usuario.find({ 'datos_personales.email': user.email })
  .then((rawResponse) =>{

    if (rawResponse.length) return res.send({ success: false, message: 'Lo sentimos, esta cuenta de correo ya fue registrada', data: 'email'})

    Usuario.create(query, (err, nuevo_usuario) => {
      if(err){
        console.log('error:', err)
        return res.send({ success: false, message: err, data: null })
      }

      if (nuevo_usuario) {
        const token = createJwt(nuevo_usuario, '0.5h')
        sendVerificationEmail(nuevo_usuario, token)
        return res.send({ success:true, message: 'Nuevo usuario creado', data: null})
        // return res.send(sendVerificationEmail(nuevo_usuario, token))
      }
      return  res.status(404).send({ success:false, message: 'Error al crear el usuario', data: [] })
    });
  })
  .catch((err) => {
    console.log(err)
    return res.status(500).send('Error en la peticion');
  });
});

/* PATCH update user */
router.patch('/update', isAuthenticated, (req, res, next) => {

  const data = req.body;
  const query = {
    $set: {
      'datos_personales.nombres':          data.nombre,
      'datos_personales.apellidos':        data.apellido,
      'datos_personales.telefono':         data.telefono,
      'domicilio.num':                     data.num,
      'domicilio.calle':                   data.calle,
      'domicilio.colonia':                 data.colonia,
      'domicilio.estado':                  data.estado,
      'domicilio.municipio':               data.municipio,
      'domicilio.localidad':               data.localidad,
      'domicilio.codigo_postal':           data.cp,
      'domicilio.referencias':             data.referencias
    }
  }
  Usuario.findOneAndUpdate({ '_id': req.userId }, query, { new:true }, (err, user) => {
    if(!user){
      return res.status(404).send({ success: false, message: 'Error al actualizar', data: [] });
    }
    return res.json({ success:true, message:'Datos guardados', data: user })
  });
});

router.patch('/update-password', isAuthenticated, (req, res, next) => {
  const data = req.body
  const oldPass = shajs('sha256').update(data.password).digest('hex')
  const newPass = shajs('sha256').update(data.newPassword).digest('hex')

  const query = {
    $set: {
      'datos_personales.password': newPass
    }
  }
  Usuario.findOneAndUpdate({ '_id': req.userId, 'datos_personales.password': oldPass }, query, (err, user) => {
    if(!user){
      return res.send({ success: false, message: 'Contraseña incorrecta', data: [] });
    }
    return res.json({ success:true, message:'Contraseña actualizada correctamente.', data: [] })
  });
})

/* POST Send reset password */
router.post('/password/send-reset-token', (req, res, next) => {
  const token = randomKey()
  const query = {
    $set: {
      'reset_password': {
        'token': token,
        'expires_at': dayjs(new Date()).add(30, 'minute')
      }
    }
  }

  /* Funcion asincrona como callback, solo funciona en produccion por el modulo de envio de mail */
  Usuario.findOneAndUpdate({ 'datos_personales.email': req.body.email }, query, { new:true }, async (err, user) => {
    if (err) return res.send({ success: false, message: 'Error consultando datos', data: {} })
    if (!user) return res.send({ success: false, message: 'Esta cuenta de email no se encuentra registrada en nuestro sistema.', data: { type: 'email' } })
    // send email
    try {
      const mailResponse = await sendResetPassword(user.datos_personales.email, token)
      console.log('mailresponse:', mailResponse) 
      if(mailResponse.success) {
       return res.send({ success: true, message: `Email enviado: ${token} a ${user.datos_personales.email}`, data: user })
      } else {
       return res.send({ success: false, message: `Error al enviar email: ${mailResponse.error}`, data: [] })
      }
    } catch (error) {
     console.log('erro en get', error)
     return res.send({ success: false, message: `Catch error: ${mailResponse.error}`, data: [] })
    }
  })
})

/* GET validar reset token */
router.get('/password/reset/:token', (req, res, next) => {
  // Valida que exista usuario con el token proporcionado y que no haya expirado el tiempo para cambiarlo
  const query = {
    "reset_password.token": req.params.token,
    "reset_password.expires_at": { $gte: new Date() }
  }
  Usuario.findOne(query, (err, user) => {
    if (err) return res.send({ success: false, message: 'Error consultando datos', data: [] })
    if (!user) return res.send({ success: false, message: 'Not Found', data: [] })
      console.log('user', user)
    return res.send({ success: true, message: '', data: user })
  })
})

/* POST cambiar password */
router.post('/password/reset', (req, res, next) => {
  // Nuevo password, recibe en body email de usuario a cambiar y nuevo pass
  const newPass = shajs('sha256').update(req.body.password).digest('hex')

  const query = {
    $set: {
      'datos_personales.password': newPass,
      "reset_password.expires_at": new Date()
    }
  }

  Usuario.findOneAndUpdate({ '_id': req.body.id }, query, (err, user) => {
    if (err) return res.send({ success: false, message: 'Error consultando datos', data: [] })
    if(!user) return res.send({ success: false, message: 'No se encontró el usuario', data: [] })
    return res.json({ success:true, message:'Contraseña acutalizada correctamente.', data: [] })
  })
})

/*GET user info by token*/
router.get('/me', isAuthenticated, (req, res, next) => {
  Usuario.findById(req.userId, (err, user) => {
    if (err) {
      console.log('error:', err)
      return res.send({ success: false, message: err, data: { errorLevel: 'user' } })
    }

    if (user && user.status.toLowerCase() === 'activo') {
      return res.send({ success: true, message: '', data: user })
    } else {
      return res.send({ success: false, message: 'Error consultando los datos del usuario', data: { errorLevel: 'user' } })
    }


  })
})

router.get('/email/verify/:token', async (req, res) => {
  const token = req.params.token || null
  if (!token) return res.status(409).json({ success: false, message: 'Código no válido', data: null })

  try {
    const decoded = jwt.verify(token, jwtKey)
    const id = decoded.id
    const user = await Usuario.findByIdAndUpdate(id, { $set: { email_verified_at: new Date() }}, { new: true })

    if (user) {
      const token = createJwt(user)
      return user.status.toLowerCase() === 'activo'
        ? res.json({ success: true, message: 'Bienvenido', data: token })
        : res.json({ success: false, message: 'Su cuenta se encuentra inactiva, contacte con soporte', data: [] })
    } else {
      return res.status(409).json({ success: false, message: 'No se encontró el usuario', data: { errorLevel: 'token', errorType: err.name } })
    }
  } catch (err) {
    return res.status(409).send({ success: false, message: jwtHandleError(err), data: { errorLevel: 'token', errorType: err.name } })
  }
})

router.post('/email/verification-notification', async (req, res) => {
  const email = req.body.email
  if (!email) return res.status(409).json({ success: false, message: 'Error', data: null })
  try {
    const user = await Usuario.findOne({ "datos_personales.email": email })
    if (user) {
      // console.log(user)
      if (user.email_verified_at) {
        return res.send({ success:false, message: 'Esta cuenta ya ha sido verificada', data: user})
      } else {
        const token = createJwt(user, '0.5h')
        sendVerificationEmail(user, token)
        return res.send({ success:true, message: 'Email enviado', data: null})
      }
    } else {
      return res.send({ success:false, message: 'El email ingresado no se encuentra en nuestros registros.', data: null})
    }
  } catch (err) {
    return res.status(409).json({ success: false, message: err, data: null })
  }
})

router.get('/testing-email-verify', (req, res) => {
  // const ran = randomKey()
  // console.log(ran)
  return res.send(sendVerificationEmail(null, createJwt({ _id: 554878 }, '0.5h')))
})

function createJwt(user, expiresIn = '4h') {
  return jwt.sign({ id: user._id }, jwtKey, { expiresIn })
}

function randomKey() {
  let key = ""
  const code = "Aa1Bb2Cc3Dd4Ee5Ff6Gg7Hh8Ii9Jj0KkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz"

  for (x = 0;x < 6;x++){
    key = key.concat(code.substr(Math.floor(Math.random() * code.length), 1))
  }
  
  return key.concat(Date.now().toString())
}

// TODO: refactor para utilizar directo de authMiddleware
function jwtHandleError (err) {
  switch(err.name) {
    case 'TokenExpiredError': return 'La sesión ha expirado';
    case 'JsonWebTokenError': return err.message;
    default: return 'Error verificando los datos de sesión';
  }
}

module.exports = router;
